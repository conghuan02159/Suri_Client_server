'use strict';

const fun_product = require('./functions/func_chat');
const path = require('path');
const uploadDir = path.join('./uploads/');
var fs = require('fs');



    module.exports = io => {
    io.on('connection', function(socket) {
        console.log(" co nguoi ket noi " + socket.id);
        // console.log( socket.adapter.rooms );
        var ArrayIdOnl = [];
        var clients;



        socket.on('connectedapp', function (idUser) {
           // console.log(" room connected : " +ArrayIdOnl.length + " -- idUser connected " + idUser);
            console.log(" da online  : " +ArrayIdOnl.length );
            console.log("out socket room : connectedapp"+ socket.id);
        });

        socket.on('disconnectedapp', function (idUser) {
            console.log("out socket room : disconnectedapp"+ socket.id);
            console.log(idUser);
            console.log(ArrayIdOnl.indexOf(idUser));
            //xoa mot thang trong mang
            ArrayIdOnl.splice(ArrayIdOnl.indexOf(socket.Username),1);

        });

        socket.on('registeruserchat', function (nameroom,userId1,nameUser1,userId2,nameUser2) {

                socket.join(nameroom);
                socket.emit("returnroom",{ nameroom: nameroom , IdUser : userId2, nameUser: nameUser2});

        });

        socket.on('outroomchat', function (nameroom) {
            console.log("out name room : "+ nameroom);
            console.log("out socket room : "+ socket.id);
            console.log("--------------------------------");

            socket.leave(nameroom,function (err) {

            });
            io.to(nameroom).emit("leaveroom",{leaveroom : nameroom});
        });

    socket.on('new message', function (nameroom, idsender ,idreceiver,messageimage,messagetext) {
            console.log("out socket room : new messag"+ socket.id);

            var d = new Date();
            var timeStamp = d.getTime();
            clients = 0;
            clients = io.sockets.adapter.rooms[nameroom];
            // console.log(io.sockets.adapter.rooms[nameroom]);

            if(clients.length !=2){
                console.log("send notification ..................................")
                fun_product.push_notification_chat(nameroom, idsender, idreceiver,messageimage,messagetext,timeStamp);
                console.log("goi thong bao messages : " +idsender + "  idreceiver  " +idreceiver );
            }else{

                console.log("gui chat : " + idsender);

                console.log("messagetext: " +messagetext);

                var resutl = fun_product.SendChatMessage(nameroom, idsender,idreceiver,messageimage,messagetext,timeStamp,io);
                if(resutl!=false){
                    console.log("success!");
                }else{
                    console.log("fall!")

                }
            }
        });


        // when the client emits 'typing', we broadcast it to others
        socket.on('typing', function (nameroom,idsender,namesender) {
            io.to(nameroom).emit('typing', { idsender: idsender,namesender:namesender
            });
            console.log(" typing : " + idsender)
        });

        // log(" stop typing : " + user)
        // when the client emits 'stop typing', we broadcast it to others
        socket.on('stop typing', function (nameroom,idsender,namesender) {
            io.to(nameroom).emit('stop typing', {
                idsender: idsender,namesender:namesender
            });
            console.log(" stop typing : " + idsender)
        });

        socket.on('getDataChat', function (nameroom,page,idreceiver) {
                console.log(" -- nameroom,page,idreceiver " + nameroom +" -- "+ page +" --- "+ idreceiver)

            fun_product.getDataChat(nameroom,page,idreceiver,socket);
        });

    });


    function randomString(length)
    {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_";
        for( var i=0; i < length; i++ )
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        return text;
    }

    function getBase64Image(imgData) {
        return imgData.replace(/^data:image\/png;base64,/, "");
    }



};