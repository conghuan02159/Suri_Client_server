const auth = require('basic-auth');
const jwt = require('jsonwebtoken');
const profile = require('./functions/profile');
const password = require('./functions/password');
const config = require('./config/config.json');
const registerSuri = require('./functions/register');
const loginSuri = require('./functions/login');
const insertSuri = require('./functions/insert_suri');

const outputSuri = require('./functions/output_suri');
const formidable = require('formidable');
const path = require('path');
const uploadDir = path.join('./uploads/');
// const formidable = require('formidable');
/*const formidable = require('formidable');*/
var fs = require('fs');
const comments = require('./functions/comments');
const rep_comment = require('./functions/reply_comment');
const insuri = require('./functions/show-suridetail');
const home = require('./functions/homesuri');
const display_view = require('./functions/display_view');
const upphone = require('./functions/update_phone');
const homepost = require('./functions/homepost');
const noti_sos = require('./functions/notification_sos');
const notifications = require('./functions/notifications');
const search_fixer = require('./functions/search_fixer');
const search_repair = require('./functions/search_repair');
const search_community = require('./functions/search_community');
const introduce = require('./functions/introduce');
const statistic = require('./functions/statistic_sos');
const category = require('./functions/category');
const  startEvent = require('./functions/start_run_event');
const  versionApp = require('./functions/version');

const func_chat = require('./functions/func_chat');
 // update


module.exports = router => {
    router.get('/', (req, res) => res.end('Welcome to suri !'));



    /*-------------------------------------------------------------------
                                    User
    --------------------------------------------------------------------*/

    router.get('/notification', (req, res) => {
        home.push_messtotopic("csmqPKYzHuE:APA91bEB-2Vt5-a8jKaoc7zxmnuPBNZJfT6KuESHe_F2dpPbzanFWfAU-vaDPylqACt3oOnJby9B612Q3_Q_ynaC2uICZ1iGTPBydnuzbN0w-5P4vx25DPdkyLEOImpAtW_OCSGFqsEN"
            , "12345")
            .then(result => {
                res.status(result.status).json({
                    // homesuri: result.homesuris
                });
            })
            .catch(err => res.status(err.status).json({request: err.request_check, message: err.message}));
    });

    router.get('/HomeSuri', (req, res) => {
        home.getHomeSuri()
            .then(result => {
                res.status(result.status).json({
                    homesuri: result.homesuris
                });
            })
            .catch(err => res.status(err.status).json({request: err.request_check, message: err.message}));
    });

    router.post('/showboxMessage', (req, res) => {
        const user_id = req.body.user_id;
        const page = req.body.page;

        if (!user_id) {
            res.status(400).json({message: "Invalid Request !"});
        } else {

            func_chat.getboxMessage(user_id,page)
                .then(result => {
                    res.json(result)
                    console.log("result ........... : "+result);

                })
                .catch(err => res.status(err.status).json({message: err.message}));
        }
    });


    router.post('/notificationdriver', (req, res) => {
        const id_customer = req.body.id_customer;
        const id_driver = req.body.id_driver;
        var d = new Date();
        var timeStamp = d.getTime();

        if (!id_customer || !id_driver ) {
            res.status(400).json({request: 2, message: 'Invalid Request !'});
        } else {
            noti_sos.notification_send_driver(id_customer, id_driver)
                .then(result => {
                    // const token = jwt.sign(result, config.secret, {expiresIn: 1440});
                    res.status(result.status).json({
                        messagetype : result.messagetype,
                        infor: result.infor

                    });

                })
                .catch(err => res.status(err.status).json({request: err.request_check, message: err.message}));
        }
    });

    router.post('/notidriverrequest', (req, res) => {
        const id_customer = req.body.id_customer;
        const id_driver = req.body.id_driver;
        const booleansdri = req.body.booldriver;
        var d = new Date();
        var timeStamp = d.getTime();
        if (!id_customer || !id_driver) {
            res.status(400).json({request: 2, message: 'Invalid Request !'});
        } else {
            noti_sos.notification_send_customer(id_customer, booleansdri,id_driver)
                .then(result => {
                    // const token = jwt.sign(result, config.secret, {expiresIn: 1440});
                    res.status(result.status).json({
                        messagetype : result.messagetype,
                        infor: result.infor
                    });
                })
                .catch(err => res.status(err.status).json({request: err.request_check, message: err.message}));
        }
    });
// remove working in customer
    router.post('/notiremovefromdriver', (req, res) => {
        const id_customer = req.body.id_customer;
        const id_driver = req.body.id_driver;
        const reason = req.body.reason;

        var d = new Date();
        var timeStamp = d.getTime();
        if (!id_customer || !id_driver) {
            res.status(400).json({request: 2, message: 'Invalid Request !'});
        } else {
            noti_sos.notification_remove_from_driver(id_customer, id_driver, reason)
                .then(result => {
                    // const token = jwt.sign(result, config.secret, {expiresIn: 1440});
                    res.status(result.status).json({
                        messagetype : result.messagetype,
                        infor: result.infor
                    });
                })
                .catch(err => res.status(err.status).json({request: err.request_check, message: err.message}));
        }
    });

    // Hiển thị danh sach thông báo

    router.post('/display_user_notify', (req, res) => {
        const user_id = req.body.user_id;

        if(!user_id){
            res.status(400).json({message: "Invalid Request !"});
        }else{
            notifications.listNotify(user_id)

                .then(result => {
                    res.json({notifications : result});
                })

                .catch(err => {
                    res.status(err.status).json({message: err.message});
                })

        }

    });
    //login one user

    router.post('/login_one_user', (req, res) =>{
        const user_id = req.body.user_id;
        const token = req.body.token;
        const version = req.body.version;
        console.log("token senddddd ............ version :: " + token)

        if(!user_id || !version || version == null){

            res.status(400).json({message: "Invalid Request !"});

        }else{

            notifications.loginOneUser(user_id,token,version)

                .then(result => {


                    res.json(result);
                })

                .catch(err => res.status(err.status).json({ response: err.response, message : err.message}));
        }
    });

    // Lấy danh sách token

    router.post('/list_token', (req, res) =>{
        const suri_id = req.body.suri_id;

        if(!suri_id){
            res.status(400).json({message: "Invalid Request !"});
        }else{
            notifications.listToken(suri_id)

                .then(result => {
                    res.json(result);
                })

                .catch(err => res.status(err.status).json({message : err.message}));
        }
    });

    // Lấy thông báo của user

    router.post('/get_notify_user', (req, res) =>{
        const user_id = req.body.user_id;
        const page = req.body.page;

        if(!user_id){
            res.status(400).json({message: "Invalid Request !"});
        }else{
            notifications.notifySuri(user_id,page)

                .then(result => {
                    res.json(result);
                })

                .catch(err => res.status(err.status).json({ response: err.response, message : err.message}));
        }
    });

    router.post('/PostHomePublic', (req, res) => {
        const user_id = req.body.user_id;
        const status = req.body.status;
        const image = req.body.image;
        const description = req.body.description;
        const code_suri = req.body.code_suri;
        const name = req.body.name;
        const type = req.body.type;
        var d = new Date();
        var timeStamp = d.getTime();

        if (!user_id || !status || !image || !description.trim() || !name.trim() || !type) {
            res.status(400).json({request: 2, message: 'Invalid Request !'});
        } else {
            homepost.HomePostPublic(user_id, status, image, description, code_suri, name, type)
                .then(result => {
                    // const token = jwt.sign(result, config.secret, {expiresIn: 1440});
                    res.status(result.status).json({
                        status: result.user_id
                    });
                })
                .catch(err => res.status(err.status).json({request: err.request_check, message: err.message}));
        }
    });

// login

    router.post('/loginsuri', (req, res) => {
        const credentials = auth(req);
        if (!credentials) {
            res.status(400).json({message: 'Invalid Request !'});
        } else {
            loginSuri.login_user(credentials.name, credentials.pass)

                .then(result => {
                    // const token = jwt.sign(result, config.secret, {expiresIn: 1440});
                    res.status(result.status).json({
                        phone: result.phone,
                        name: result.name,
                        userid: result.userid,
                        state: result.state,
                        user_type: result.user_type,
                        job: result.job,
                        image:result.avatar,
                        check_block: result.check_block
                    });
                })
                .catch(err => res.status(err.status).json({request: err.request_check, message: err.message}));
        }
    });

    // update avatar facebook
    router.post('/update_face_avatar', (req, res) => {

        const image_link = req.body.image_link;
        const user_id = req.body.user_id;


        // console.log("login facebook: name " + name + " face_id: " + face_id + " lat: "
        //     + lattitude + " long: " + longitude + " userType: " + userType + " token: " + token);

        if (!image_link || !user_id || !image_link.trim() || !user_id.trim()) {

            res.status(400).json({message: 'Invalid Request !'});

        } else {
            console.log("first login facebook: userId " + user_id+"/n"+ " image_link: "+image_link );
            loginSuri.updateAvatarFacebook(user_id, image_link)
                .then(result => {
                    res.setHeader('Location', '/facebook_users/' + user_id);
                    res.status(result.status).json({
                        userid: result.userid

                    })
                })

                .catch(err => res.status(err.status).json({ message: err.message}));
        }
    });
    // login with facebook
    router.post('/facebook_users', (req, res) => {

        const name = req.body.name;
        const face_id = req.body.face_id;
        const lattitude = req.body.latitude;
        const longitude = req.body.longitude;
        const userType = req.body.user_type;
        const DevIDShort_check = req.body.DevIDShort_check;
        const AndroidID_check = req.body.AndroidID_check;
        const szImei_check = req.body.szImei_check;
        const version = req.body.version;
        const token = req.body.token;

        if (!name || !face_id || !name.trim() || !face_id.trim()
            || !userType || !userType.trim() || !AndroidID_check.trim() || !DevIDShort_check.trim() || !version.trim() || version == null) {
            res.status(400).json({message: 'Invalid Request !'});
        } else {
            loginSuri.loginFaceBook(face_id, name, lattitude, longitude, userType, token,DevIDShort_check,AndroidID_check,szImei_check,version)
                .then(result => {
                    res.setHeader('Location', '/facebook_users/' + face_id);
                    res.status(result.status).json({
                        userid: result.userid,
                        name: result.name,
                        user_type: result.userType,
                       state: result.state,
                        phone: result.phone,
                        message: result.message,
                        check_block: result.check_block

                    })
                })

                .catch(err => res.status(err.status).json({ message: err.message}));
        }
    });
    // Kiểm tra phone number
    router.post('/check_number', (req, res) => {

        const userid = req.body.user_id;
        const phonenumber = req.body.phone;

        if (!userid || !phonenumber) {

            res.status(400).json({request: 2, message: 'Invalid Request !'});

        } else {
            profile.checkNumber(userid, phonenumber)
                .then(result => {

                    var  val = Math.floor( Math.random() * (999999 - 100000)) +100000;

                    registerSuri.sendsms(userid, phonenumber, val, "", "", 1);

                    res.setHeader('Location', '/usersuri/' + phonenumber);
                    res.status(result.status).json({
                        request: result.request_check, message: result.message
                    })
                })

                .catch(err => res.status(err.status).json({request: err.request_check, message: err.message}));
        }
    });

    /*-------------------------------------------------------------------
                                    Profile and Repair
    --------------------------------------------------------------------*/


    // Hiển thị chi tiết thông tin User
    router.post('/showprofile', (req, res) => {
        const user_id = req.body.user_id;

        if (!user_id) {
            res.status(400).json({message: "Invalid Request !"});
        } else {
            profile.getProfile(user_id)

                .then(result => {
                    res.json(result)
                })

                .catch(err => res.status(err.status).json({message: err.message}));
        }
    });


    // Đăng ký trở thành thợ sửa chữa
    router.post('/become_prepaire', (req, res) => {
        const user_id = req.body.user_id;
        const address = req.body.address;
        const fix_job = req.body.fix_job;
        const description = req.body.description;
        const other_jobs = req.body.other_jobs;

         if (!user_id) {
             res.status(400).json({message: "Invalid Request !"});
        } else {
             profile.becomeRepaire(user_id, address, fix_job, description ,other_jobs )
                .then(result => {
                    res.json({address :result.address, fix_job: result.fix_job, description: result.description, other_jobs:result.other_jobs});
                })
                .catch(err => {
                    res.status(err.status).json({message: err.message})
                })
        }
    });


    // Hiển thị danh sách thợ sửa

    router.post('/list_repairer', (req, res) => {

        const skip = parseInt(req.body.skip);
        const fix_job = req.body.fix_job;
        const id = req.body.id;
        const address = req.body.address;

        if (skip < 0 || skip == null || id == null) {
            res.status(400).json({message: "Invalid Request !"})
        } else {
            profile.listRepairer(fix_job,skip,id, address)

                .then(result => res.json(result))

                .catch(err => res.status(err.status).json({message: err.message}));
        }

    });

    //edit profile
    router.post('/editprofile', (req, res) =>{
        const user_id = req.body.user_id;
        const name = req.body.name;
        const birthday = req.body.birthday;
        const job = req.body.job;
        const address =req.body.address;
        const user_type =req.body.type;
        const nameImg = req.body.image;
        const description = req.body.description;

        // 0  null image -------------- 1 exist image
        if(user_type != "0" && nameImg != null && nameImg != "" ){
            fs.unlink(uploadDir + nameImg, (err) => {
                if (err) {
                    throw err;
                }
            });
        }
        if (!user_id) {
            res.status(400).json({message: "Invalid Request !"});
        }else {

            profile.editprofile(user_id,name,birthday,job,address,user_type,nameImg, description)

                .then(result => res.status(result.status).json({ name: result.name, birthday: result.birthday, job: result.job, address: result.address, user_type : result.user_type, description : result.description
                }))
                .catch(err => res.status(err.status).json({ message: err.message }));

        }
    });


    // Hien thi danh sach tho sua trong ban kinh 10km

    router.post('/list_repairer_radius', (req, res) => {
        // const skip = parseInt(req.body.skip);
        const user_id = req.body.user_id;

        profile.listRepaierRadius(user_id)

            .then(result => res.json({members: result}))

            .catch(err => res.status(err.status).json({message: err.message}));

    });

    // Nhật ký hoạt động

    router.post('/ViewlogActivity', (req, res) => {
        const idUser = req.body.user_id;
        if (!idUser) {
            res.status(400).json({message: "Invalid Request !"});
        } else {

            profile.ViewlogActivity(idUser)

                .then(result => {
                    res.json({ viewlog:result})

                })

                .catch(err => res.status(err.status).json({message: err.message}));
        }
    });

    // Update token

    router.post('/update_token', (req, res) => {
        const user_id = req.body.user_id;
        const token = req.body.token;

        if (!user_id || !token) {
            res.status(400).json({message: "Invalid Request !"});
        } else {
            profile.updateToken(user_id, token)
                .then(result => res.status(result.status).json({ message: result.message }))
                .catch(err => res.status(err.status).json({ message: err.message }));
        }
    });



    // Url upload image avatar
    router.post('/upload_avatar', (req, res) => {

        uploadImageAvatar(req, res);

    });

    function uploadImageAvatar(req, res) { // This is just for my Controller same as app.post(url, function(req,res,next) {....
        const form = new formidable.IncomingForm();

        form.multiples = true;
        form.keepExtensions = true;
        form.uploadDir = uploadDir;

        form.parse(req, (err, fields, files) => {

            var fileee = files.file;

            if (err) return res.status(500).json({error: err});

            profile.uploadAvatar(fields.user_id, fileee.path.substring(8))
                .then(result =>
                    res.status(200).json({image : result.image}))

        });

        form.on('fileBegin', function (name, file) {
            const [fileName, fileExt] = file.name.split('.');
            file.path = path.join(uploadDir, `${fileName}_${new Date().getTime()}.${fileExt}`)

        })
    }




    /*-------------------------------------------------------------------
                                    Register
    --------------------------------------------------------------------*/
// register suri
    router.post('/usersuri', (req, res) => {

        const name = req.body.name;
        const phonenumber = req.body.phone;
        const password = req.body.password;
        const lattitude = req.body.lattitude;
        const longitude = req.body.longitude;
        const userType = req.body.user_type;
        const DevIDShort_check = req.body.DevIDShort_check;
        const AndroidID_check = req.body.AndroidID_check;
        const szImei_check = req.body.szImei_check;
        const version = req.body.version;
        const token = req.body.token;


        if (!name || !phonenumber || !password || !name.trim() || !phonenumber.trim() || !password.trim()
            || !userType || !userType.trim() || !AndroidID_check.trim() || !szImei_check.trim() || !version.trim() || version.trim() == null) {

            res.status(400).json({request: 2, message: 'Invalid Request !'});

        } else {
            // request check: 1: user da duoc dang ky, 2: lỗi server, 3: phone number đã được sử dụng
            registerSuri.registerSuri(name, phonenumber, password, userType, lattitude, longitude, token,DevIDShort_check,AndroidID_check,szImei_check,version)
                .then(result => {

                    var  val = Math.floor( Math.random() * (999999 - 100000)) +100000;

                    registerSuri.sendsms(result.userid, phonenumber, val, "", "", 1);

                    res.setHeader('Location', '/usersuri/' + phonenumber);
                    res.status(result.status).json({
                        request: result.request_check, userid: result.userid, name: result.name,
                        phone: result.phone, message: result.message
                    })
                })

                .catch(err => res.status(err.status).json({request: err.request_check, message: err.message}));
        }
    });


// verificode yêu cầu kiểm tra verificode -> xác nhận phone
    router.post('/request_code', (req, res) => {
        const user_id = req.body.user_id;
        const code = req.body.code;

        if (!user_id || !code || !user_id.trim() || !code.trim()) {
            res.status(400).json({message: 'Invalid Request !'});
        } else {
            registerSuri.requestCode(user_id, code)
                .then(result => {
                    res.setHeader('Location', '/request_code/' + user_id);
                    res.status(result.status).json({request: result.request_check, check_event: result.check_event, message: result.message})
                })

                .catch(err => res.status(err.status).json({request: err.request_check, message: err.message}));
        }
    });


// resend code
    router.post('/resend_code', (req, res) => {
        const user_id = req.body.user_id;

        if (!user_id || !user_id.trim()) {

            res.status(400).json({message: 'Invalid Request !'});

        } else {

            registerSuri.ReSendSms(user_id)

                .then(result => res.json(result))

                .catch(err => res.status(err.status).json({message: err.message}));
        }
    });

    // Forgot password: quên mật khẩu
    //XÁC NHẬN PHONE
    // send verificode

    router.post('/request_phone', (req, res) => {
        const phone = req.body.phone;

        if (!phone || !phone.trim()) {

            res.status(400).json({message: 'Invalid Request !'});

        } else {

            var  val = Math.floor( Math.random() * (999999 - 100000)) +100000;

            password.requestPhone(phone, val)

                .then(result => {

                    res.setHeader('Location', '/request_phone/' + phone);
                    res.status(result.status).json({
                        response: result.responValue,
                        userid: result.userId,
                        message: result.message
                    })
                })

                .catch(err => res.status(err.status).json({message: err.message}));
        }
    });

    // xác nhận verificode
    router.post('/request_code_forgot', (req, res) => {
        const user_id = req.body.user_id;
        const code = req.body.code;

        if (!user_id || !code || !user_id.trim() || !code.trim()) {

            res.status(400).json({message: 'Invalid Request !'});

        } else {

            password.requestCode(user_id, code)

                .then(result => {

                    res.setHeader('Location', '/request_code_forgot/' + user_id);

                    res.status(result.status).json({request: result.request_check, message: result.message})
                })

                .catch(err => res.status(err.status).json({message: err.message}));
        }
    });
    // reset pass
    router.post('/reset_password', (req, res) => {
        const user_id = req.body.user_id;
        const pass = req.body.password;

        if (!user_id || !pass || !user_id.trim() || !pass.trim()) {

            res.status(400).json({message: 'Invalid Request !'});

        } else {
            //

            password.resetPass(user_id, pass)

                .then(result => {

                    res.setHeader('Location', '/reset_password/' + user_id);
                    res.status(result.status).json({request: result.request_check, message: result.message})
                })

                .catch(err => res.status(err.status).json({errr: " hahaha", message: err.message}));
        }
    });


    /*-------------------------------------------------------------------
                                Suri (Post)
    --------------------------------------------------------------------*/

    // insert suri tab bài viết
    router.post('/insertsuri_fix', (req, res) => {

        const user_id = req.body.user_id;
        const status = req.body.status;
        const description = req.body.description;
        const address = req.body.address;
        const longitude = req.body.longitude;
        const latitude = req.body.latitude;
        const category  = req.body.category;




        if (!user_id || !status || !description ||  !user_id.trim() || !status.trim() || !description.trim()) {

            res.status(400).json({message: 'Invalid Request !'});

        } else {

            insertSuri.insertSuri_fix(user_id, status, description, latitude, longitude, address, category)
          //  insertSuriTow.insertSuri_fix(user_id, status, description, latitude, longitude, address)
                .then(result => {
                    res.json({tokenArray : result.tokenArray, suri_id : result.suri_id});
                })

                .catch(err => res.status(err.status).json({message: err.message}));
        }
    });

    // Update Suri tab Bài viết

    router.post('/updatesuri_fix', (req, res) => {
        const suri_id = req.body.suri_id;
        const status = req.body.status;
        const description = req.body.description;
        var listitem = req.body.listimgdel;
        var arrImgDel = [];

        if(listitem.size != undefined || listitem != "")
        {
            arrImgDel = listitem.split(" , ");
            if (arrImgDel.length > 0 || !listitem || listitem == "") {
                for (var i = 0; i < (arrImgDel.length); i++) {
                    fs.unlink(uploadDir + arrImgDel[i]);
                }
            }
        }

        if (!suri_id || !status || !description) {
            res.status(400).json({message: "Invalid Request !"});
        } else {
            insertSuri.updateSuri_fix(suri_id, status, description ,arrImgDel)

                .then(result =>
                    res.json({status:result.status, description: result.description}))
                .catch(err => {
                    res.status(err.status).json({message: err.message});
                })
        }
    });

    // Insert Suri in Communiti
    router.post('/insertsuri_com', (req, res) => {

        let user_id = req.body.user_id;
        const status = req.body.status;
        const description = req.body.description;
        const name = req.body.name;
        const linkshow = req.body.linkshow;
        let code = req.body.code;

        if (!user_id) {
            user_id = "001001010101010101010111";
        };

        if (!code) {
            // noinspection JSAnnotator
            code = "0";
        };


        if (!user_id || !status ||  !name  || !user_id.trim() || !status.trim()  || !name.trim()) {

            res.status(400).json({message: 'Invalid Request !'});

        } else {

            insertSuri.insertSuri_community(user_id, status, description, name, code,linkshow)
            // insertSuriTow.insertSuri_community(user_id, status, description, name, code)
                .then(result => {

                    res.setHeader('Location', '/post_suri/' + user_id);
                    res.status(result.status).json({suri_id:result.suri_id,
                        message: result.message
                    })
                })

                .catch(err => res.status(err.status).json({message: err.message}));
        }
    });

    // update View
    router.post('/update_view', (req, res) => {
        const suri_id = req.body.suri_id;
        const countView = req.body.count_view;

        if (!suri_id || !suri_id.trim()) {

            res.status(400).json({message: 'Invalid Request !'});

        } else {

            insertSuri.updateView(suri_id, countView)

                .then(result => res.json(result))

                .catch(err => res.status(err.status).json({message: err.message}));
        }
    });

    // Update suri tab Cộng đồng
    router.post('/updatesuri_com', (req, res) => {
        const suri_id = req.body.suri_id;
        const status = req.body.status;
        const description = req.body.description;
        const name = req.body.name;
        // let code = req.body.code;


        var listitem = req.body.listimgdel;
        var arrImgDel = [];

        if(listitem != "0")
        {

            arrImgDel = listitem.split(" , ");

            if (arrImgDel.length > 0 || !listitem || listitem == "") {
                for (var i = 0; i <= (arrImgDel.length - 1); i++) {
                fs.unlink(uploadDir + arrImgDel[i], (err) => {
                    if (err) throw err;
                        console.log('successfully deleted /image/' + arrImgDel[i]);
                    });
                }
            }
        }


        if (!suri_id || !status || !description || !name || !status.trim() || !description.trim() || !name.trim()) {

            res.status(400).json({message: "Invalid Request !"});

        } else {
            insertSuri.updateSuri_community(suri_id, status, description, name, arrImgDel)

                .then(result => {
                    res.status(result.status).json({message: result.message});
                })

                .catch(err => {
                    res.status(err.status).json({message: err.message});
                })
        }
    });

    /*
    *  upload and get image from folder upload
    *  example: url: http://localhost:8080/api/suri/uploads/20170831_152810_1504584856073.jpg
    * */

    router.post('/delete_suri', (req, res) => {
        const suri_id = req.body.suri_id;
        // const code_delete = req.body.code_delete;

        if (!suri_id) {
            res.status(400).json({message: "Invalid Request !"});
        }
        else {
            insertSuri.deleteSuri(suri_id)

                .then(result => {
                    res.status(result.status).json({message: result.message});
                })

                .catch(err => {
                    res.status(err.status).json({message: err.message});
                })
        }

    });

    router.post('/upload_suri', (req, res) => {

        uploadImage(req, res);

    });

    // get image

    router.get('/uploads/:file', function (req, res) {
        file = req.params.file;

        var img = fs.readFileSync(uploadDir + file);
        res.writeHead(200, {'Content-Type': 'image/jpg'});
        res.end(img, 'binary');

    });


    function uploadImage(req, res) { // This is just for my Controller same as app.post(url, function(req,res,next) {....
        const form = new formidable.IncomingForm();
        form.multiples = true;
        form.keepExtensions = true;
        form.uploadDir = uploadDir;
        form.parse(req, (err, fields, files) => {
            if (err) return res.status(500).json({error: err});
            console.log(files.image.path.substring(8));
            insertSuri.uploadsuri(fields.suri_id, files.image.path.substring(8));
            res.status(200).json({respone: 2, suri_id: fields.suri_id})
        });
        form.on('fileBegin', function (name, file) {
            const [fileName, fileExt] = file.name.split('.');
            file.path = path.join(uploadDir, `${fileName}_${new Date().getTime()}.${fileExt}`)

        })
    }

    router.post('/upload_image_chat', (req, res) => {

        uploadImageChat(req, res);
});

    function uploadImageChat(req, res) { // This is just for my Controller same as app.post(url, function(req,res,next) {....
        const form = new formidable.IncomingForm();
        form.multiples = true;
        form.keepExtensions = true;
        form.uploadDir = uploadDir;
        form.parse(req, (err, fields, files) => {
            if (err) return res.status(500).json({error: err});
        console.log("file........................."+files.messageimage.path.substring(8));
        var nameimage = files.messageimage.path.substring(8);
        // var d = new Date();
        // var timeStamp = d.getTime();
        // func_chat.insertDataMongod(fields.nameroom,fields.idsender,fields.idreceiver,fields.messageimage.path.substring(8),fields.messagetext,timeStamp)
        // insertSuri.uploadsuri(fields.suri_id, files.image.path.substring(8));

        res.status(200).json({nameroom : fields.nameroom , messageimage: nameimage})
    });
        form.on('fileBegin', function (name, file) {
            const [fileName, fileExt] = file.name.split('.');
            file.path = path.join(uploadDir, `${fileName}_${new Date().getTime()}.${fileExt}`)

        })
    }




// out put

    router.post('/output_suri', (req, res) => {

        const user_id = req.body.user_id;

        if (!user_id || !user_id.trim()) {

            res.status(400).json({message: 'Invalid Request !'});

        } else {

            outputSuri.outputSuri(user_id)

                .then(result => res.json(result))

                .catch(err => res.status(err.status).json({message: err.message}));
        }
    });

    // Chi tiết bài viết (Chi tiet suri)

    router.post('/suridetail', (req, res) => {
        const suri_id = req.body.suri_id;

        if (!suri_id) {
            res.status(400).json({message: "Invalid Request !"});
        } else {
            insuri.showSuri(suri_id)

                .then(result => {
                    res.json(result)
                })

                .catch(err => res.status(err.status).json({message: err.message}));
        }
    });

    // Hiển thị bình luận trong bài viết

    router.post('/comment', (req, res) => {
        const suri_id = req.body.suri_id;
        const skip = parseInt(req.body.skip);

        if (skip < -1 || !suri_id ) {
            res.status(400).json({message: "Invalid Request !"});
        } else {
            insuri.showComment_InSuri(suri_id, skip)

                .then(result => {
                    res.json(result)
                })

                .catch(err => res.status(err.status).json({message: err.message}));

        }

    })

    /*-------------------------------------------------------------------
                                    Show Home
    --------------------------------------------------------------------*/

    router.post('/home_community', (req, res) => {

        const skip = parseInt(req.body.skip);

        if (skip < 0 || skip == null) {
            res.status(400).json({message: "Invalid Request !"})
        } else {
            insuri.showSuri_community(skip)

                .then(homeresult => {
                    // res.json(result)
                    res.json({homeresult})
                })

                .catch(err => res.status(err.status).json({request: err.request_check, message: err.message}));
        }

    });


    router.post('/show_forum_repair', (req, res) => {

        const category = req.body.category;
        const skip = req.body.skip;

        console.log(category,skip);

        if (!category) {

            res.status(400).json({message: 'Invalid Request !'});
        } else {
            insuri.show_forum_repair(category,skip)
                .then(result => res.json(result))

                .catch(err => res.status(err.status).json({message: err.message}));
        }

    });


    router.post('/showSuri_repair', (req, res) => {

        const skip = parseInt(req.body.skip);
        const keyCategory = req.body.category;

        insuri.showSuri_repair(skip, keyCategory)
            .then(result => {

                res.status(result.status).json({
                    surihomess: result.surihomess
                });
            })
            .catch(err => res.status(err.status).json({request: err.request_check, message: err.message}));

        if (skip < 0) {
            res.status(400).json({message: "Invalid Request !"});
        } else {
            insuri.showSuri_repair(skip, keyCategory)

                .then(result => res.json(result))

                .catch(err => res.status(err.status).json({message: err.message}));
        }

    });


    /*-------------------------------------------------------------------
                                    Comment
    --------------------------------------------------------------------*/
    // Hiển thị chi tiết bình luận

    router.post('/commentdetail', (req, res) => {
        const comment_id = req.body.comment_id;
        const skip = req.body.skip;

        if (skip == null || !comment_id) {
            res.status(400).json({status: "Dữ liệu không tồn tại"});
        } else {
            comments.commentDetail(comment_id, skip)

                .then(result => {
                    res.json(result)
                })

                .catch(err => res.status(err.status).json({message: err.message}));
        }

    });


    // Thêm bình luận noti Array

    router.post('/addcomment', (req, res) => {
        let user_id = req.body.user_id;
        const suri_id = req.body.suri_id;
        const status = req.body.status;
        let code_comment = req.body.code_comment;
        const name = req.body.name;

        if (!user_id) {
            user_id = "001001010101010101010111";
        }
        if (!code_comment) {
            code_comment = "0";
        }

        if (!user_id || !suri_id || !status || !code_comment || !name) {
            res.status(400).json({message: "Invalid Request !"});
        } else {
            comments.addComment(user_id, suri_id, status, code_comment, name)

                .then(result => {

                    res.status(result.warning).json({

                        comment_id: result.comment_id,
                        user_id : result.user_id,
                        suri_id : result.suri_id,
                        status : result.status,
                        code_comment : result.code_comment,
                        name : result.name,
                        create_at : result.create_at,
                        response: result.response,
                        message: result.message

                    })
                })

                .catch(err => res.status(err.status).json({message: err.message}));
        }

    });


    // Cập nhật bình luận

    router.post('/updatecomment', (req, res) => {
        const comment_id = req.body.comment_id;
        let code_comment = req.body.code_comment;
        const status = req.body.status;

        if (!code_comment) {
            code_comment = "0";
        }

        if (!comment_id || !code_comment || !status) {
            res.status(400).json({message: "Dữ liệu không tồn tại"});
        } else {
            comments.updateComment(comment_id, code_comment, status)

                .then(result => {
                    res.status(result.status).json({message: result.message})
                })

                .catch(err => res.status(err.status).json({message: err.message}));
        }

    });

    // Xóa bình luận

    router.post('/deletecomment', (req, res) => {
        const comment_id = req.body.comment_id;
        let code_comment = req.body.code_comment;
        const suri_id = req.body.suri_id;
        const user_id = req.body.user_id;

        if(!code_comment){
            code_comment = "0";
        }

        if (!comment_id || !code_comment) {
            res.status(400).json({message: "Invalid Request !"});
        } else {

            comments.deleteComment(comment_id, code_comment, suri_id, user_id)

                .then(result => {
                    res.status(result.status).json({message: result.message})
                })

                .catch(err => res.status(err.status).json({message: err.message}));

        }

    });


    /*-------------------------------------------------------------------
                                    Reply Comment
    --------------------------------------------------------------------*/

    // Thêm bình luận trả lời (Reply comment)

    router.post('/addrepcomment', (req, res) => {
        const comment_id = req.body.comment_id;
        let user_id = req.body.user_id;
        const suri_id = req.body.suri_id;
        const status = req.body.status;
        let code_reply = req.body.code_reply;
        const name = req.body.name;

        if (!user_id) {
            user_id = "001001010101010101010111";
        }
        if (!code_reply) {
            code_reply = "0";
        }

        if (!comment_id || !user_id || !suri_id || !status || !code_reply || !name) {
            res.status(400).json({message: "Dữ liệu không tồn tại !"});
        } else {
            rep_comment.addRepComment(comment_id, user_id, suri_id, status, code_reply, name)

                .then(result => {
                    res.status(result.warning).json({
                        comment_id: result.comment_id,
                        comment_rep_id: result.comment_rep_id,
                        user_id : result.user_id,
                        suri_id : result.suri_id,
                        status : result.status,
                        code_reply : result.code_reply,
                        name : result.name,
                        create_at : result.create_at,
                        message: result.message})
                })

                .catch(err => res.status(err.status).json({message: err.message}));

        }

    });


    router.post('/updaterepcomment', (req, res) => {
        const rep_comment_id = req.body.rep_comment_id;
        let code_reply = req.body.code_reply;
        const description = req.body.description;

        if (!code_reply) {
            code_reply = "0";
        }


        if (!rep_comment_id || !code_reply || !description) {
            res.status(400).json({message: "Dữ liệu không tồn tại !"});
        } else {
            rep_comment.updateRepComment(rep_comment_id, code_reply, description)

                .then(result => {
                    res.status(result.status).json({message: result.message});
                })

                .catch(err => {
                    res.status(err.status).json({message: err.message});
                })
        }

    });
    router.post('/deleterepcomment', (req, res) => {
        const rep_comment_id = req.body.rep_comment_id;
        let code_reply = req.body.code_reply;
        if (!code_reply) {
            code_reply = "0";
        }

        if (!rep_comment_id || !code_reply) {
            res.status(400).json({message: "Invalid Request !"});
        } else {
            rep_comment.deleteRepComment(rep_comment_id, code_reply)

                .then(result => {
                    res.status(result.status).json({message: result.message});
                })

                .catch(err => {
                    res.status(err.status).json({message: err.message});
                })
        }

    });
    //search user suri
    router.post('/search_fixer', (req, res) => {
        const keysearch = req.body.keysearch;
        const page = req.body.page;

        search_fixer.mSearch(keysearch,page)
            .then(result => res.json(result))

            .catch(err => res.status(err.status).json({message: err.message}));

    });
    //search suri
    router.post('/search_repair', (req, res) => {
        const keysearch = req.body.keysearch;
        const page = req.body.page;

        search_repair.uSearch(keysearch,page)
            .then(result => res.json(result))

            .catch(err => res.status(err.status).json({message: err.message}));

    });



    //search community
    router.post('/search_community', (req, res) => {
        const keysearch = req.body.keysearch;
        const page = req.body.page;

        search_community.cSearch(keysearch,page)
            .then(result => res.json(result))

            .catch(err => res.status(err.status).json({message: err.message}));

    });

    // introduce code mã giới thiệu
    router.post('/introduce_code', (req, res) => {
        const user_id = req.body.user_input;
        const code_introduce = req.body.code_introduce;

     //   phonenumber, user_input, code_introduce

        if (!user_id || !user_id.trim()) {

            res.status(400).json({message: 'Invalid Request !'});

        } else {

            introduce.introduc_input(user_id, code_introduce)

                .then(result => {

                    res.setHeader('Location', '/introduce_code/' + user_id);
                    res.status(result.status).json({request: result.request_check, message: result.message})
                })

                .catch(err => res.status(err.status).json({request: err.request_check, message: err.message}));
        }
    });

    // get infomation share
    router.post('/infomation_share', (req, res) => {
        const user_id = req.body.id_user;

        //   phonenumber, user_input, code_introduce

        if (!user_id || !user_id.trim()) {

            res.status(400).json({message: 'Invalid Request !'});

        } else {

            introduce.infomation_pay(user_id)

                .then(result => {

                    res.setHeader('Location', '/introduce_code/' + user_id);
                    res.status(result.status).json({time_code_introduce: result.time_code_introduce, money: result.money, message: result.message})
                })

                .catch(err => res.status(err.status).json({request: err.request_check, message: err.message}));
        }
    });

// count call sos
    router.post('/count_sos', (req, res) => {

        const countSos = req.body.count_sos;
        const countScan = req.body.count_scan;
        const  countMessenger = req.body.count_message;



        statistic.statisticSos(countSos, countScan, countMessenger)

                .then(result => {

                    res.setHeader('Location', '/count_sos/' + count);
                    var datee = new Date();
                    datee.setTime(result.created_at);
                    var strDate = datee;
                    console.log("yyyyyyyyyyyyy "+datee);
                    res.status(result.status).json({request: result.count_sos, created_at: strDate, message: result.message})
                })

                .catch(err => res.status(err.status).json({request: err.request_check, message: err.message}));

    });
//    count notification
    router.post('/count_notification', (req, res) => {
        const user_id = req.body.user_id;
        const count = req.body.count;

        if (!user_id || !user_id.trim()) {

            res.status(400).json({message: 'Invalid Request !'});

        } else {

            notifications.countNotification(user_id, count)

                .then(result => {

                    res.setHeader('Location', '/count_notification/' + user_id);
                    res.status(result.status).json({request: result.request_check, message: result.message})
                })

                .catch(err => res.status(err.status).json({request: err.request_check, message: err.message}));
        }
    });

    router.post('/count_notification_chat', (req, res) => {
        const user_id = req.body.user_id;
        const count = req.body.count_chat;

        if (!user_id || !user_id.trim()) {

            res.status(400).json({message: 'Invalid Request !'});

        } else {

            notifications.countChatNotification(user_id, count_chat)

                .then(result => {

                    res.setHeader('Location', '/count_notification/' + user_id);
                    res.status(result.status).json({request: result.request_check, message: result.message})
                })

                .catch(err => res.status(err.status).json({request: err.request_check, message: err.message}));
        }
    });

    // Update phonenumber

    router.get('/update_phonenumber', (req, res) => {

        upphone.Update_PhoneNumber()

            .then(result => res.status(result.status).json(result))

            .catch(err => res.status(err.status).json({message: err.message}));

    });

    // Display view

    router.post('/UpView', (req, res) => {

        display_view.mUpView()

            .then(result => res.status(result.status).json(result))

            .catch(err => res.status(err.status).json({message: err.message}));

    });

    // history notification

    router.post('/history_notification', (req, res) => {
        const user_id = req.body.user_id;
        const page = req.body.page;


        notifications.historynotification(user_id,page)

            .then(result => res.json(result))

            .catch(err => res.status(err.status).json({message: err.message}));

    });

    router.post('/view_notification', (req, res) => {
        const user_senders = req.body.user_sender;
        const suri_ids = req.body.suri_id;
        const type_notifications = req.body.type_notification;

        console.log('' + user_senders+ ' ----- ' + suri_ids + " ---  " + type_notifications);

        notifications.view_notifications(user_senders,suri_ids,type_notifications)

            .then(result => res.json(result))

            .catch(err => res.status(err.status).json({message: err.message}));

    });
    router.post('/add_category', (req, res) => {
        const category_id = req.body.category_id;
        const name = req.body.name;
        const count_comment = req.body.count_comment;
        const count_post = req.body.count_post;


        category.registerCategory(category_id, name, count_post, count_comment)

            .then(result =>
                res.json(result))

            .catch(err => res.status(err.status).json(
                {message: err.message}));

    });

    router.post('/show_count_category', (req, res) => {

        category.show_category()


            .then(result => {

                res.status(result.status).json({
                    category: result.category
                });
            })
            .catch(err => res.status(err.status).json({request: err.request_check, message: err.message}));

    });
    router.post('/start_event', (req, res) => {

        const  check = req.body.check_event;
        startEvent.startSuri(check)

            .then(result => {

                res.status(result.status).json({
                    check_event: result.check_event
                });
            })
            .catch(err => res.status(err.status).json({request: err.request_check, message: err.message}));

    });

    router.post('/check_event', (req, res) => {


        startEvent.responseCheckEvent()

            .then(result => {

                res.status(result.status).json({
                    check_event: result.check_event
                });
            })
            .catch(err => res.status(err.status).json({request: err.request_check, message: err.message}));

    });
//  number 1 : spam bảng tin
//  number 2 : spam tao tai khoản
//  number 3 : spam comment
//  number 4 : update X

    router.post('/disconnect_user', (req, res) => {
        const  phone_number = req.body.phone_number;
        const  number_spam = req.body.number_spam;
        const  block_user = req.body.block_user;

        notifications.disconnect_user(phone_number,number_spam,block_user)

            .then(result => {

                res.status(result.status).json({
                    message: result.message
                });
            })
            .catch(err => res.status(err.status).json({request: err.request_check, message: err.message}));

    });

    router.post('/check_version', (req, res) => {


        versionApp.CheckVersion()

            .then(result => {

                res.status(result.status).json(result);
            })
            .catch(err => res.status(err.status).json({request: err.request_check, message: err.message}));

    });

}


