'use strict';
const mongoose = require("./connect");

const Schema = require("mongoose/lib/schema");

// const Schema = mongoose.Schema;

// table detail notification
const detail_notificationSchema_tow = mongoose.Schema({

    notification_id         : {type: mongoose.Schema.ObjectId, ref: 'notification_tow'},
    user_id             	: {type: mongoose.Schema.ObjectId, ref: 'suriuser'},
    watching	        	: String,
    create_at           	: String

});

mongoose.Promise = global.Promise;
module.exports = mongoose.model('detail_notification_tow', detail_notificationSchema_tow);
