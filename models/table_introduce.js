'use strict';
const mongoose = require("./connect");

const Schema = require("mongoose/lib/schema");

// table user
const suriIntroduceSchema = mongoose.Schema({

    user_id: {type: mongoose.Schema.ObjectId, ref: 'suriuser'},
    user_input: {type: mongoose.Schema.ObjectId, ref: 'suriuser'},
    code_introduce    : String,  // 0 :  chưa nhập, 1 nhập thành công
    check_pay : Number,
    created_at : String

});

mongoose.Promise = global.Promise;
module.exports = mongoose.model('introduce', suriIntroduceSchema);
