'use strict';
const mongoose = require("./connect");

const Schema = require("mongoose/lib/schema");
// table suri
const suriSchema = mongoose.Schema({

    user_id					: [{type: mongoose.Schema.ObjectId, ref: 'suriuser'}],
    status					: String,
    images					: [String],
    description				: String,
    linkshow                : String,
	link                    : String,
    code		        	: String,
    name                    : String,
    address                  : String,
    view                    : Number,
    category                : {type: mongoose.Schema.ObjectId, ref: 'category'},
    location                : {
        'type' : {String, default: "Point"},
        coordinates : [Number]
    },
    temp_password_time		: String,
	type                    : String,
    tax                     : {type: String, default: 'post' },
    number_comment          : Number,
    created_at				: String,
    updated                  : String

});

mongoose.Promise = global.Promise;

module.exports = mongoose.model('suri', suriSchema);