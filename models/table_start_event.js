'use strict';
const mongoose = require("./connect");

const Schema = require("mongoose/lib/schema");

// const Schema = mongoose.Schema;

// table detail notification
const startEvent = mongoose.Schema({

    check_event         : Boolean,
    create_at           	: String

});

mongoose.Promise = global.Promise;
module.exports = mongoose.model('start_event', startEvent);
