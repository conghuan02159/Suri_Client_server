'use strict';
const mongoose = require("./connect");

const Schema = require("mongoose/lib/schema");

// table user
const categorySchema = mongoose.Schema({

    name_category                : String,
    count_comment      	: Number,
    count_post		: Number,
    create_at           : String

});

mongoose.Promise = global.Promise;
module.exports = mongoose.model('category', categorySchema);


