'use strict';
const mongoose = require("./connect");

const Schema = require("mongoose/lib/schema");

// table user
const statisticSchema = mongoose.Schema({

    count_call_sos				: Number,
    count_scan              : Number,
    count_message           : Number,
    created_at           : String,
    date_created            : String

});

mongoose.Promise = global.Promise;
module.exports = mongoose.model('statistic', statisticSchema);

