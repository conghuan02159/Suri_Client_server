'use strict';

const mongoose = require("./connect");
const Schema = require("mongoose/lib/schema");


const chatSchema = mongoose.Schema({
    room                 :String,
    idsender             : {type: Schema.Types.ObjectId, ref: 'suriuser'},
    idreceiver           : {type: Schema.Types.ObjectId, ref: 'suriuser'},
    created_at_box       : String,
    messages             : [{
        messageimage     : { type: String },
        id_user_send     : { type: Schema.Types.ObjectId, ref: 'suriuser' },
        // latitude         :  String,
        // longitude        :  String,
        messagetext      : {type : String },
        created_at       : String
    }]
});


mongoose.Promise = global.Promise;

module.exports = mongoose.model('chat', chatSchema);