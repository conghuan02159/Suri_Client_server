'use strict';
const mongoose = require("./connect");

const Schema = require("mongoose/lib/schema");

// table user
const notificationall = mongoose.Schema({

    notification_id : {type: mongoose.Schema.ObjectId, ref: 'notification'},
    title_all  	    : String,
    status_all      : String,
    image_all	    : String,
    create_at_all	: String

});

mongoose.Promise = global.Promise;
module.exports = mongoose.model('notification_all', notificationall);

