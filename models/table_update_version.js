'use strict';
const mongoose = require("./connect");

const Schema = require("mongoose/lib/schema");

// const Schema = mongoose.Schema;

// table detail notification
const updateVersion = mongoose.Schema({

    version         : String,
    create_at           	: String

});

mongoose.Promise = global.Promise;
module.exports = mongoose.model('version', updateVersion);
