'use strict';
const mongoose = require("./connect");

const Schema = require("mongoose/lib/schema");

// table user
const notificationSchema = mongoose.Schema({

    suri_id             : {type: mongoose.Schema.ObjectId, ref: 'suri'},
    number_user         : Number,
    type_notification	: String,
    create_at           : String,
    user_create  	    : {type: mongoose.Schema.ObjectId, ref: 'suriuser'},
    user_sender         : {type: mongoose.Schema.ObjectId, ref: 'suriuser'},
    watching            : String,
    last_time           : String,
    last_person         : [{type: mongoose.Schema.ObjectId, ref: 'suriuser'}],
    interactive         : Boolean

});

mongoose.Promise = global.Promise;
module.exports = mongoose.model('notification', notificationSchema);

