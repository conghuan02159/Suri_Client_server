'use strict';
const mongoose = require("./connect");

const Schema = require("mongoose/lib/schema");

// table user
const suriUserSchema = mongoose.Schema({

    face_id				: String,
    name				: String,
    phone_number		: String,
    birthday            : String,
    address             : String,
    job                 : String,
    hashed_password		: String,
    token				: String,
    image				: String,
    created_at			: String,
    created_at_type		: String,
    other_jobs			: String,
    temp_password		: String,
    temp_password_time	: String,
    user_type			: String,
    DevIDShort_check    : String,
    AndroidID_check     : String,
    szImei_check        : String,
    location            :{
        'type': { type: String, default: 'Point' },
        coordinates: [Number]
    },
    state				: String,
    fix_job             : String, // field điền ngành nghề chuyên môn
    description         : String, // field để mô tả kỹ năng của người thợ
    time_code_introduce    :Number, // SỐ Lần nhập mã giới thiệu
    money                   :Number,
    check_input_code    : Number,  // 0 :  chưa nhập, 1 nhập rồi
    check_block         :Boolean,
    version             :String


});

mongoose.Promise = global.Promise;
module.exports = mongoose.model('suriuser', suriUserSchema);
