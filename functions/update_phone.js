'use strict';
const user = require('../models/table_user');


exports.Update_PhoneNumber = () =>

    new Promise((resolve, reject) => {

        user.find({})
            .then(result => {
                if (result.length > 0) {
                    for(var i = 0 ; i < result.length;i++){
                        var rows = result[i];
                        var phone = rows.phone_number;
                        rows.phone_number = phone.replace("0838","02838");
                        rows.save();
                    }
                    resolve({status: 201, message: "Update phone number Success:"});

                } else {
                    reject({status: 404, message: "User Not Found !"});
                }
            })

            .catch(err => reject({status: 500, message: "Internal Server Error !"}));

    });