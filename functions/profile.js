'use strict';

const user = require('../models/table_user');
const suri = require('../models/table_suri');
const ObjectId = require("mongodb").ObjectID;
const comment = require('../models/table_comment');

// Hiển thị thông tin user
exports.getProfile = (user_id) =>

    new Promise((resolve,reject) => {
        let ObjectId;
        ObjectId = require("mongodb").ObjectId;

        user.find({ "_id": ObjectId(user_id) }, {"hashed_password" : 0, "token" : 0})

            .then(users => {
                if(users.length === 0){
                    reject({status: 404, message: "User not found !"})
                }else{
                    return users[0];
                }
            })

            .then(user => resolve(user))

            .catch(err => reject({ status: 500, message: 'Internal Server Error !' }))

    });

//Đăng ký thành thợ sửa
exports.becomeRepaire = (user_id, address, fix_job, description, other_jobs) =>
    new Promise((resolve, reject) => {
        const d = new Date();
        const timeStamp = d.getTime();
        user.find({"_id" : ObjectId(user_id)})

            .then(u => {
                let repaier = u[0];
                const usertype = u[0].user_type;
                // 0 chua 1 co--------

                    if (fix_job ==5){
                        console.log("job = 5---------"+ other_jobs);
                        repaier.address = address;
                        repaier.fix_job = fix_job;
                        repaier.description = description;
                        repaier.user_type = "1";
                        repaier.other_jobs = other_jobs;
                        repaier.created_at_type = timeStamp;
                        repaier.save();
                    }
                    else {
                        console.log("job != 5");

                        repaier.address = address;
                        repaier.fix_job = fix_job;
                        repaier.description = description;
                        repaier.user_type = "1";
                        repaier.created_at_type = timeStamp;

                    return repaier.save();

                }

            })

            .then(() => {
                resolve({status: 200,address :address, fix_job: fix_job, description: description, created_at_type :timeStamp , other_jobs:other_jobs  }) // Trở thành thợ sửa thành công
            })

            .catch(err => {
                reject({status: 500, message : "Internal Server Error !"})
            })

    });

// Hiển thị danh sách thợ sửa

exports.listRepairer = (fix_job,skip,id, address) =>

    new Promise((resolve, reject) => {
    // show address
    if(address != null){

        user.find({"user_type" : "1", "fix_job" : fix_job, 'address': {  $regex :  address  }},{"hashed_password" : 0, "token": 0, "__v" : 0})
            .limit(10)
            .skip(10 * skip)
            .then(users => {

                let count = users.length;
                let repaierArray = [];

                for(let i = 0; i<count; i++){
                    if(users[i]._id != id){
                        repaierArray.push(users[i]);
                    }
                }

                resolve(repaierArray);
            })
            .catch(err => reject({status: 500, message: "Internal Server Error !"}));


    } // show to address
    else{
        user.find({"user_type" : "1", "fix_job" : fix_job},{"hashed_password" : 0, "token": 0, "__v" : 0})
            .limit(10)
            .skip(10 * skip)
            .then(users => {

                let count = users.length;
                let repaierArray = [];

                for(let i = 0; i<count; i++){
                    if(users[i]._id != id){
                        repaierArray.push(users[i]);
                    }
                }

                resolve(repaierArray);
            })
            .catch(err => reject({status: 500, message: "Internal Server Error !"}));

    }

        // user.find({"user_type" : "1", "fix_job" : fix_job, 'address': {  $regex :  address  }},{"hashed_password" : 0, "token": 0, "__v" : 0})
        //     .limit(10)
        //     .skip(10 * skip)
        //     .then(users => {
        //
        //         let count = users.length;
        //         let repaierArray = [];
        //
        //         for(let i = 0; i<count; i++){
        //             if(users[i]._id != id){
        //                 repaierArray.push(users[i]);
        //             }
        //         }
        //
        //         resolve(repaierArray);
        //     })
        //     .catch(err => reject({status: 500, message: "Internal Server Error !"}));

    });


// Hiển thị danh sách thợ sửa trong bán kính 10km

exports.listRepaierRadius = (user_id) =>

    new Promise ((resolve, reject) => {

        let ObjectId;
        ObjectId = require("mongodb").ObjectId;

        console.log("OK");

        let milesToRadian = function(kilomet){
            let earthRadiusInKilomet = 6378.1;
            return kilomet / earthRadiusInKilomet;
        };

        user.findOne({"_id": user_id})

            .then(user_one => {

                let coor = user_one.location.coordinates;
                console.log(coor);

                user.find({
                    "location" : {
                        $geoWithin: {
                            $centerSphere: [coor, milesToRadian(10) ]
                        }
                    }
                })

                    .then(us => resolve(us))

            })

            .catch(err => reject({status: 500, message: "Internal Server Error !"}));

    });

// edit profile

exports.editprofile = (user_id, name, birthday, job, address ,user_type, nameImg, description) =>
    new Promise((resolve, reject) => {
        user.find({"_id": ObjectId(user_id)})
            .then(surie => {
                if (surie.length != 0) {
                    let newProfile = surie[0];
                    newProfile.name = name;
                    newProfile.birthday = birthday;
                    newProfile.job = job;
                    newProfile.address = address;
                    newProfile.description = description;

                    if (user_type != "1" && nameImg != null && nameImg != "") {
                        user.findOneAndUpdate({"_id": ObjectId(user_id)}, {$pull: {image : nameImg}})
                            .then( result => {
                                resolve({status: 200, name :result.name, birthday : result.birthday, job : result.job, address : result.address, description : result.description});
                            })
                            .catch(err => reject({status: 500, message: "Internal Server Error !"}));
                    }

                    newProfile.save()

                        .then(result => {

                            resolve({status: 200, name : result.name, birthday : result.birthday, job : result.job, address : result.address, description: result.description});
                        })

                    // .then(result => })

                } else {
                    reject({ status: 401, message: 'User not found !' });
                }
            })

            .catch(err => reject({ status: 500, message: 'Internal Server Error !' }));

    });



// Hiển thị lịch sử hoạt động
exports.showHistory = (user_id) =>
    new Promise((resolve,reject) => {
            let ObjectId = require("mongodb").ObjectId;
            suri.find({"user_id" : ObjectId(user_id)},{"_id" : 1, "status" : 1, "created_at" : 1, "tax" : 1})

                .then(hiss1 => {
                    comment.find({"user_id" : ObjectId(user_id)},{"_id" : 1, "status" : 1, "create_at" : 1, "tax" : 1})

                        .then(hiss2 => {
                            let hiss3 = hiss1.concat(hiss2);
                            hiss3.sort();
                            hiss3.reverse();
                            if(hiss3){
                                return hiss3;
                            }
                        })

                        .then(hiss3 => resolve(hiss3))
                })

                .catch(err => reject({status: 500, message: "Internal Server Error !"}));
        }
    );

// Update Token User

exports.updateToken = (user_id, token) =>
    new Promise((resolve,reject) => {
            console.log(user_id);
            console.log(token);

            let ObjectId = require("mongodb").ObjectId;

            user.update({"_id" : ObjectId(user_id)},{$set : {"token" : token}},{safe: true, upsert: true, new: true})

                .then(result => resolve({ status: 200, message: 'Update Susscesfully! '}))

                .catch(err => reject({ status: 500, message: 'Internal Server Error !' }));
        }
    );

// Check phone number (yes or no)
exports.checkNumber = (userid, phonenumber) =>
    new Promise((resolve,reject) => {

            let ObjectId = require("mongodb").ObjectId;

            user.find({"phone_number" : phonenumber})

                .then(users => {
                    console.log(users);
                    if(users.length === 0){

                        user.update({"_id" : ObjectId(userid)},{$set : {"phone_number" : phonenumber}},{safe: true, upsert: true, new: true})

                            .then(result => resolve({status: 200,request_check : 0, message: "Update Susscesfully!"}))

                            .catch(err => reject({ status: 500,request_check: 1, message: 'Internal Server Error !' }))

                    }else{

                        reject({status: 404,request_check: 1, message: "Phone is already exists"})

                    }
                })

                .catch(err => reject({ status: 500, message: 'Internal Server Error !' }));
        }
    );

// Hiển thị lịch sử hoạt động

exports.ViewlogActivity = (idUser) =>
    new Promise((resolve, reject) =>{

    // POST
    console.log("NHẬT KÝ HOẠT ĐỘNG : "+idUser+" (ID_USER)");
        suri.find({"user_id": ObjectId(idUser)},{_id:1, user_id:1, status:1, created_at:1, type:1})
            .then(post=>{
                let arrPost =[];
                for (let i =0; i<post.length;i++){

                    if(post[i].type =="1"){

                        arrPost.push(post[i]);
                    }
                }
                console.log("POST: "+arrPost.length);

                // COMMENT
                comment.find( { "user_id": ObjectId(idUser)},{_id:1,suri_id:1, user_id:1,status:1,create_at: 1}).sort({create_at: -1})
                    .populate({path: "suri_id" ,select: "_id type"})
                    .then(suir_id =>{
                        let arsrCMT =[];
                        for (let i =0 ; i<suir_id.length;i++){

                            if (suir_id[i].suri_id.type==1){
                                arsrCMT.push(suir_id[i]) ;

                            }
                        }
                        let arr = arsrCMT,
                                result = arr.filter(function (a) {
                                    let key = a.suri_id + '|' + a.suri_id;
                                    if (!this[key]) {
                                        this[key] = true;
                                        return true;
                                    }
                                }, Object.create(null));
                        console.log("COMMENT: "+result.length);

                        resolve({status: 200, post: arrPost, cmt: result});

                    });

            })
            .catch(err => reject({ status: 500, message: err.message }));
    });

// insert avatar
exports.uploadAvatar = (user_id, image) =>
    new Promise((resolve, reject) => {
        user.find({"_id": ObjectId(user_id)})

            .then(us => {

                if (us.length === 0) {

                    reject({status: 404, message: "User Not Found !"});

                } else {
                    let a = us[0]
                    a.image = image;

                    a.save();
                    return us[0];

                }
            })
            .then(userPush => {
                console.log("userPush"+userPush.image);
                resolve({status: 200,image :  userPush.image});
            })
            .catch(err => reject({status: 500, message: "Internal Server Error !"}));

    });

