'use strict';
const notification = new require('../models/table_notification');
const suri = new require('../models/table_suri');
const comment = new require('../models/table_comment');
let ObjectId = require("mongodb").ObjectId;



exports.deleteNotification = (user_id, suri_id, type_notification,comment_id) =>
    new Promise((resolve, reject) => {
        const d = new Date();
        const timeStamp = d.getTime();
        console.log("suri id in notification  " + suri_id);

        //kiem tra id suri trong bang thong bao da ton tai chua ?
        comment.find({
            suri_id: ObjectId(suri_id),
            user_id : ObjectId(user_id)
        }, function (err, result) {
            if (err) {
                throw err;
            } else {
                if(result!=null){
                    console.log('co user con comment ' +result.length);
                }else{
                notification.deleteOne({
                    "suri_id": ObjectId(suri_id),
                    "user_sender": ObjectId(user_id),
                    "type_notification": type_notification
                }, function (err, result) {
                    if (err) {
                        console.log('Error updating notification object: ' + err);
                    } else {
                        console.log('' + result.toString() + ' delete  ');
                    }
                });

                notification.count({
                    "suri_id": ObjectId(suri_id),
                    "type_notification": type_notification,
                    "interactive": true
                }, function (error, count) {
                    console.log("count .................. " + count + "   " + suri_id + "  " + type_notification);

                    notification.updateMany({"suri_id": ObjectId(suri_id), "type_notification": type_notification},
                        {$set: {"number_user": count}}, function (err, result) {
                            if (err) {
                                console.log('Error updating notification object: ' + err);

                            } else {
                                console.log(".....................update table notification success !!!");
                            }
                        });


                });
            }}

        });
    });

// add bang thong bao
exports.addNotification = (user_id, suri_id, type_notification,interactive) =>
    new Promise((resolve, reject) => {
        const d = new Date();
        const timeStamp = d.getTime();
        console.log("suri id in notification  " + suri_id);

        //kiem tra id suri trong bang thong bao da ton tai chua ?
        notification.findOne({
            suri_id: ObjectId(suri_id),
            type_notification: type_notification,
            user_sender : ObjectId(user_id)
        }, function (err, result) {
            if (err) {
                throw err;
            } else {
                //  da ton tai : no la user admin -> update interactive = true va count number update all
                // new khac user : count number all va update all time
                if (result != null) {


                    notification.updateOne({"user_sender" : ObjectId(user_id),"user_create" : ObjectId(user_id) ,"type_notification" : type_notification, "interactive" : false},
                        {$set: {"interactive":"true"}},function (err, result) {
                            if (err) {
                                console.log('Error updating notification object: ' + err);
                            }else{
                                console.log('' + result.toString() + ' updated admin bai viet  interactive = true');
                            }
                        });

                    notification.updateOne({"suri_id": ObjectId(suri_id),"user_sender" : ObjectId(user_id) ,"type_notification" : type_notification},
                        {$set: {"last_time":timeStamp}},function (err, result) {
                            if (err) {
                                console.log('Error updating notification object: ' + err);
                            }else{
                                console.log('' + result.toString() + ' updated time admin ');
                            }
                        });


                    notification.count({"suri_id" : ObjectId(suri_id),"type_notification" : type_notification ,"interactive" : true}, function (error, count) {
                        console.log("count .  ................. " + count+ "   " + suri_id + "  " + type_notification );


                        notification.updateMany({"suri_id" : ObjectId(suri_id),"type_notification" : type_notification},
                            {$set: {"number_user": count,"create_at":timeStamp}}, function (err, result) {
                                if (err) {
                                    console.log('Error updating notification object: ' + err);
                                } else {
                                    console.log(' updating notification lai tat ca count: ' + result);
                                    const arrlast_person = [];
                                    notification.find({"suri_id":ObjectId(suri_id),"type_notification" : type_notification,"interactive" : true},
                                        {"user_sender":1}, function (err, last_person) {
                                            if (err) {
                                                console.log('Error find last person: ' + err);
                                            } else {
                                                for(var i = 0; i < last_person.length;i++){
                                                    arrlast_person.push(last_person[i].user_sender);
                                                }
                                                // console.log(' select last person is :: ' + last_person[0].user_sender);
                                                notification.updateMany({"suri_id" : ObjectId(suri_id),"type_notification" : type_notification},
                                                    {$set: {"last_person": arrlast_person}}, function (err, result) {
                                                        if (err) {
                                                            console.log('Error updating success last_person: ' + err);
                                                        } else {
                                                            console.log(' updating success last_person ' + result);
                                                        }
                                                    });
                                            }
                                        }).limit(2).sort({"last_time":-1});
                                }
                            });

                        console.log(".....................update table notification success !!!");
                    });
                }
                else
                {
                    suri.find({"_id" : ObjectId(suri_id)})
                        .then(su => {
                            console.log("user create ......." + su.length);
                            if(su.length === 0){
                                reject({status: 400, message: "Suri not found !"});
                            }else{
                                console.log("user create ......." );

                                let notifications = new notification({
                                    suri_id: suri_id,
                                    number_user: 0,
                                    type_notification: type_notification,
                                    create_at: timeStamp,
                                    user_create: su[0].user_id,
                                    user_sender : user_id,
                                    watching : "0",
                                    last_time : timeStamp,
                                    last_person: null,
                                    interactive : interactive //true
                                });
                                notifications.save()
                                    .then(() => {
                                        notification.count({"suri_id" : ObjectId(suri_id),"type_notification" : type_notification ,"interactive" : true}, function (error, count) {
                                            console.log("count .  ................. " + count );

                                            notification.updateMany({"suri_id" : ObjectId(suri_id),"type_notification" : type_notification},
                                                {$set: {"number_user": count,"create_at":timeStamp}}, function (err, result) {
                                                    if (err) {
                                                        console.log('Error updating notification object: ' + err);

                                                    } else {

                                                        console.log('' + result.toString() + ' document(s) updated');
                                                        const arrlast_person = [];
                                                        notification.find({"suri_id":ObjectId(suri_id),"type_notification" : type_notification,"interactive" : true},
                                                            {"user_sender":1}, function (err, last_person) {
                                                                if (err) {
                                                                    console.log('Error find last person: ' + err);

                                                                } else {

                                                                    for(var i = 0; i < last_person.length;i++){
                                                                        arrlast_person.push(last_person[i].user_sender);
                                                                    }
                                                                    // console.log(' select last person is :: ' + last_person[0].user_sender);
                                                                    notification.updateMany({"suri_id" : ObjectId(suri_id),"type_notification" : type_notification},
                                                                        {$set: {"last_person": arrlast_person}}, function (err, result) {
                                                                            if (err) {
                                                                                reject({status: 409, message: err});
                                                                                console.log('Error updating success last_person: ' + err);
                                                                            } else {
                                                                                console.log(' updating success last_person ' + result);
                                                                                resolve({status: 200,  message: 'Suri Created Successfully !'});
                                                                            }
                                                                        });
                                                                }
                                                            }).limit(2).sort({"last_time":-1});
                                                    }
                                                });
                                        });
                                        console.log("................ insert table notification success !!!");
                                    })
                    //
                            }
                        })



                }
            }

        });

    });



exports.addNotificationComment = (user_id, name,suri_id, type_notification,interactive) =>
    new Promise((resolve, reject) => {
        const d = new Date();
        const timeStamp = d.getTime();
        console.log("suri id in notification  " + suri_id);

        //kiem tra id suri trong bang thong bao da ton tai chua ?
        notification.findOne({
            suri_id: ObjectId(suri_id),
            type_notification: type_notification,
            user_sender : ObjectId(user_id)
        }, function (err, result) {
            if (err) {
                throw err;
            } else {
                //  da ton tai : no la user admin -> update interactive = true va count number update all
                // new khac user : count number all va update all time

                if (result != null) {
                    notification.updateOne({"user_sender" : ObjectId(user_id),"user_create" : ObjectId(user_id) ,"type_notification" : type_notification, "interactive" : false},
                        {$set: {"interactive":"true"}},function (err, result) {
                            if (err) {
                                console.log('Error updating notification object: ' + err);
                            }else{
                                console.log('' + result.toString() + ' updated admin bai viet  interactive = true');
                            }
                        });

                    notification.updateOne({"suri_id": ObjectId(suri_id),"user_sender" : ObjectId(user_id) ,"type_notification" : type_notification},
                        {$set: {"last_time":timeStamp}},function (err, result) {
                            if (err) {
                                console.log('Error updating notification object: ' + err);
                            }else{

                                console.log('' + result.toString() + ' updated time admin ');

                            }
                        });


                    notification.count({"suri_id" : ObjectId(suri_id),"type_notification" : type_notification ,"interactive" : true}, function (error, count) {
                        console.log("count .  ................. " + count+ "   " + suri_id + "  " + type_notification );


                        notification.updateMany({"suri_id" : ObjectId(suri_id),"type_notification" : type_notification},
                            {$set: {"number_user": count,"create_at":timeStamp}}, function (err, result) {
                                if (err) {
                                    console.log('Error updating notification object: ' + err);

                                } else {
                                    console.log(' updating notification lai tat ca count: ' + result);

                                    const arrlast_person = [];
                                    notification.find({"suri_id":ObjectId(suri_id),"type_notification" : type_notification,"interactive" : true},
                                        {"user_sender":1}, function (err, last_person) {
                                            if (err) {
                                                console.log('Error find last person: ' + err);
                                            } else {
                                                for(var i = 0; i < last_person.length;i++){
                                                    arrlast_person.push(last_person[i].user_sender);
                                                }
                                                // console.log(' select last person is :: ' + last_person[0].user_sender);
                                                notification.updateMany({"suri_id" : ObjectId(suri_id),"type_notification" : type_notification},
                                                    {$set: {"last_person": arrlast_person}}, function (err, result) {
                                                        if (err) {
                                                            console.log('Error updating success last_person: ' + err);
                                                        } else {
                                                            console.log(' updating success last_person ' + result);

                                                        }
                                                    });
                                            }
                                        }).limit(2).sort({"last_time":-1});
                                }
                            });

                        console.log(".....................update table notification success !!!");

                    });

                }

                else
                {
                    suri.find({"_id" : ObjectId(suri_id)})
                        .then(su => {
                            console.log("user create ......." + su.length);
                            if(su.length === 0){
                                reject({status: 400, message: "Suri not found !"});
                            }else{
                                console.log("user create ......." );

                                let notifications = new notification({
                                    suri_id: suri_id,
                                    number_user: 0,
                                    type_notification: type_notification,
                                    create_at: timeStamp,
                                    user_create: su[0].user_id,
                                    user_sender : user_id,
                                    watching : "0",
                                    last_time : timeStamp,
                                    last_person: null,
                                    interactive : interactive //true
                                });
                                notifications.save()
                                    .then(() => {
                                        notification.count({"suri_id" : ObjectId(suri_id),"type_notification" : type_notification ,"interactive" : true}, function (error, count) {
                                            console.log("count .  ................. " + count );

                                            notification.updateMany({"suri_id" : ObjectId(suri_id),"type_notification" : type_notification},
                                                {$set: {"number_user": count,"create_at":timeStamp}}, function (err, result) {
                                                    if (err) {
                                                        console.log('Error updating notification object: ' + err);

                                                    } else {

                                                        console.log('' + result.toString() + ' document(s) updated');
                                                        const arrlast_person = [];
                                                        notification.find({"suri_id":ObjectId(suri_id),"type_notification" : type_notification,"interactive" : true},
                                                            {"user_sender":1}, function (err, last_person) {
                                                                if (err) {
                                                                    console.log('Error find last person: ' + err);

                                                                } else {

                                                                    for(var i = 0; i < last_person.length;i++){
                                                                        arrlast_person.push(last_person[i].user_sender);
                                                                    }
                                                                    // console.log(' select last person is :: ' + last_person[0].user_sender);
                                                                    notification.updateMany({"suri_id" : ObjectId(suri_id),"type_notification" : type_notification},
                                                                        {$set: {"last_person": arrlast_person}}, function (err, result) {
                                                                            if (err) {
                                                                                reject({status: 409, message: err});
                                                                                console.log('Error updating success last_person: ' + err);
                                                                            } else {
                                                                                console.log(' updating success last_person ' + result);
                                                                                resolve({status: 200,  message: 'Suri Created Successfully !'});
                                                                            }
                                                                        });
                                                                }
                                                            }).limit(2).sort({"last_time":-1});
                                                    }
                                                });
                                        });
                                        console.log("................ insert table notification success !!!");
                                    })
                                //
                            }
                        })



                }
            }

        });

    });




// add bang chi tieet thoong bao
