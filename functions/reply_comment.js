'use strict';

const rep_comment = new require('../models/table_reply_comment');
const comment = new require('../models/table_comment');
const send_notification = require('../functions/notification_send');
const suri =new require('../models/table_suri');
const user =new require('../models/table_user');
const add_table_noti = require('../functions/add_table_notification');

exports.addRepComment = (comment_id, user_id, suri_id, status, code_reply, name) =>

    new Promise((resolve, reject) => {
        const d = new Date();

        const timeStamp = d.getTime();

        console.log(comment_id+" "+user_id+"  "+suri_id+" "+status+" "+code_reply+" "+name);

        const newRepComment = new rep_comment({
            comment_id : comment_id,
            user_id    : user_id,
            suri_id	   : suri_id,
            status     : status,
            code_reply : code_reply,
            name       : name,
            create_at  : timeStamp
        });
        newRepComment.save()

            .then(() => {
                comment.findByIdAndUpdate(
                    comment_id,
                    {$push: {"rep_comment" : newRepComment._id}},
                    {safe: true, upsert: true, new: true},
                    function(err, model){
                        console.log(err);
                    }
                );

            })


            .then(() => {
                let comment_rep_id = newRepComment._id;
                let user_id = newRepComment.user_id;
                let suri_id = newRepComment.suri_id;
                let status = newRepComment.status;
                let code_reply = newRepComment.code_reply;
                let name = newRepComment.name;
                let create_at = newRepComment.create_at;


                add_table_noti.addNotification(user_id, suri_id,"2",true);

                comment.find({"_id" : comment_id})
                    .then(tbcomments =>{

                        let title = tbcomments[0].status;

                        suri.find({"_id" : suri_id})
                            .then(tbsuris =>{
                                const d = new Date();

                                const timeStamp = d.getTime();

                                let updateTime = tbsuris[0];
                                updateTime.updated = timeStamp;
                                updateTime.save();

                                let type = updateTime.type;

                                user.find({"_id" : user_id})
                                    .then(tbusers =>{
                                        let  avatar = tbusers[0].image;

                                        const message = { type_notification:"commentreply"};
                                        var infor = {comment_id: comment_id, user_id: user_id, suri_id: suri_id , type: type, title : title,
                                            status  : status, avatar: avatar ,code_reply:code_reply,name:name,create_at:create_at};
                                        send_notification.push_messcomment(suri_id,message,infor);
                                        resolve({warning: 200,
                                            comment_id : comment_id,
                                            comment_rep_id: comment_rep_id,
                                            user_id : user_id,
                                            suri_id : suri_id,
                                            status : status,

                                            code_reply : code_reply,
                                            name : name,
                                            create_at : create_at,
                                            message: "Reply comment successfully !"})
                                    });


                            });




                    });



            })

            .catch(err => reject({status: 500, message: "Lỗi Server !"}));

    });

exports.updateRepComment = (rep_comment_id, code_reply, description) =>
    new Promise ((resolve, reject) => {
        console.log(rep_comment_id+" "+code_reply+" "+description);

        let ObjectId = require("mongodb").ObjectId;

        rep_comment.find({
            "_id" : ObjectId(rep_comment_id)
        })

            .then(rep_comments => {
                if(rep_comments.length === 0){
                    reject({status: 404, message: "Không tìm thấy bình luận !"});
                }else{
                    return rep_comments[0];
                }
            })

            .then(rep_comment_one => {
                const code = rep_comment_one.code_reply;
                const id_cmt = rep_comment_one._id;
                // console.log(code+" "+id_cmt+ " "+description);
                if(rep_comment_one.user_id == "001001010101010101010111"){
                    if(code_reply === code){
                        rep_comment.update({"_id" : ObjectId(id_cmt)}, {$set: {"status" : description}})

                            .then(() => {
                                resolve({status: 200, message: "Cập nhật thành công !"});
                            })
                    }else{
                        reject({status: 401, message: "Mã code sai !"});
                    }
                }else{
                    console.log("OK");
                    if(code_reply === "0"){
                        rep_comment.update({"_id" : ObjectId(id_cmt)}, {$set: {"status" : description}})

                            .then(() => {
                                resolve({status: 200, message: "Cập nhật thành công !"});
                            })
                    }else{
                        reject({status: 401, message: "Mã code sai !"});
                    }
                }


            })

            .catch(err => reject({status: 500, message: "Lỗi Server !"}));

    });


exports.deleteRepComment = (rep_comment_id, code_reply) =>

    new Promise((resolve, reject) => {
        console.log(rep_comment_id+ " " +code_reply);
        let ObjectId = require("mongodb").ObjectId;

        rep_comment.find({
            "_id" : ObjectId(rep_comment_id)
        })

            .then(comments => {
                if(comments.length === 0){
                    reject({status: 404, message: "Không tìm thấy bình luận !"});
                    console.log("OK 1");
                }else{
                    return comments[0];
                    console.log("OK 2");
                }
            })

            .then(comment_one => {
                const code = comment_one.code_reply;
                const cmt_id = comment_one._id;

                if(comment_one.user_id == "001001010101010101010111"){
                    if(code_reply === code){

                        console.log("OK 1");

                        comment.update({"rep_comment": ObjectId(cmt_id)}, {$pullAll: {rep_comment: [ObjectId(cmt_id)]}})

                            .then(() => {
                                comment_one.remove()

                                    .then(() => {
                                        resolve({status: 200, message: "Đã xóa !"});
                                    })
                            })

                    }else{
                        reject({status: 401, message: "Mã code sai !"});
                    }
                }else{
                    if(code_reply === "0"){
                        console.log("OK 2");

                        comment.update({"rep_comment": ObjectId(cmt_id)}, {$pullAll: {rep_comment: [ObjectId(cmt_id)]}})

                            .then(() => {
                                comment_one.remove()

                                    .then(() => {
                                        resolve({status: 200, message: "Đã xóa !"});
                                    })
                            })
                    }else{
                        reject({status: 401, message: "Mã code sai !"});
                    }
                }

            })

            .catch(err => reject({status: 500, message: "Lỗi Server !"}));


    });
