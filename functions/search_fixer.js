const userSuri = require('../models/table_user');
const bcrypt = require('bcryptjs');

exports.mSearch = (keysearch,page) =>
    new Promise((resolve,reject) => {
        const limit = 10;
        if(page.is)
            if(page<1) page = 1;
        const start = (limit * page) - limit;
        console.log(keysearch);
        userSuri.find(
            {$and: [{'user_type' : "1"},{$or:[ {'name': {  $regex :  keysearch  }},{'phone_number': {  $regex :  keysearch  }}]}]}
        )

            .skip(start).limit(limit)
            .sort({created_at: -1})
            .then(product => {
                resolve({status: 200, message: product});

            })
            .catch(err => reject({status: 500, message: "Internal Server Error !"}));

    });
