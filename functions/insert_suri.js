const suri = require('../models/table_suri');
const user = require('../models/table_user');
const comment = require('../models/table_comment');
const reply_comment = require('../models/table_reply_comment');
const notification = require('../models/table_notification');
const tbCategory = require('../models/table_category');

const bcrypt = require('bcryptjs');
let ObjectId = require('mongodb').ObjectId;
const send_notification = require('../functions/notification_send');
const path = require('path');
const uploadDir = path.join('./uploads/');
var fs = require('fs');
const add_table_noti = require('../functions/add_table_notification');
// const userid_magic = ObjectId("001001010101010101010111");

// add a Suri in fix Tab

exports.insertSuri_fix = (user_id, status, description, latitude, longitude, address, category) =>

    new Promise((resolve, reject)=> {

        console.log("user_id: "+user_id+ " status: "+status+" latitude: "+latitude+ " logitude: "+longitude+" adress: "+address);
        user.find({"_id" : ObjectId(user_id)})

            .then(get_user_token =>{
                var token_main = get_user_token[0].token;

                const d = new Date();

                const timeStamp = d.getTime();

                console.log(user_id + "  " + status);

                const newSuri = new suri({
                    user_id					: user_id,
                    status					: status,
                    description				: description,
                    created_at              : timeStamp,
                    view                    : 0,
                    type                    : "1",
                    code                    : "0",
                    number_comment          : 0,
                    category                :category,
                    list_token              : token_main,
                    list_user               : user_id,
                    address                 :address,
                    location : {
                        type : "Point",
                        coordinates : [
                            longitude,latitude
                        ]
                    }
                });

                newSuri.save()

                    .then(() =>{
                        tbCategory.find({"_id": ObjectId(category)})

                            .then(categorys => {
                                let count_post = categorys[0];
                                let numberPost = count_post.count_post;

                                if (numberPost != null) {
                                    numberPost = numberPost + 1;

                                }
                                else{
                                    numberPost = 1;
                                }
                                count_post.count_post = numberPost;
                                count_post.save();
                                console.log("yyyyyyyyyyyyy "+count_post);

                            });



                        var coor = newSuri.location.coordinates;
                        const suri_in_notify = newSuri._id;
                        send_notification.push_messcomment(suri_in_notify,"","")

                        add_table_noti.addNotification(user_id, suri_in_notify,"1",false)
                        // add_table_noti.addNotification(user_id, suri_in_notify,"2",false)
                        // const newNotify = new notification({
                        //     suri_id             : suri_in_notify,
                        //     type_notification   : "Post",
                        //     number_user         : 0,
                        //     create_at           : timeStamp
                        // })
                        //
                        // newNotify.save()
                        //
                        //     .then(() => {
                        //         var notify_id = newNotify._id;
                        //
                        //
                        //         const de_Notify = new detail_notify({
                        //             notification_id     : notify_id,
                        //             user_id             : user_id,
                        //             watching            : "0",
                        //             create_at           : timeStamp
                        //         });
                        //
                        //         de_Notify.save()

                                    .then(() => {
                                        var milesToRadian = function(kilomet){
                                            var earthRadiusInKilomet = 6378.1;
                                            return kilomet / earthRadiusInKilomet;
                                        };
                                        user.find({
                                            "location" : {
                                                $geoWithin: {
                                                    $centerSphere: [coor, milesToRadian(10) ]
                                                }
                                            }
                                        },{"token" : 1, "_id" : 0})

                                            .then(us => {
                                                var count = us.length;

                                                var tokenArray = [];

                                                for(var i = 0; i<count; i++){
                                                    tokenArray.push(us[i].token);
                                                }
                                                resolve({tokenArray : tokenArray, suri_id : suri_in_notify});
                                            })
                                    // })
                            })
                    })
                    .catch(err =>{
                        if(err.code == 11000){
                            reject({status: 409, message: 'Suri Already Registered !'});
                        } else{
                            reject({status: 500, message: 'Internal Server Error !'});
                        }
                    });
            })

    });

// Update a Suri in Fix tab

exports.updateSuri_fix = (suri_id, status, description, listitem) =>

    new Promise((resolve, reject) => {
        console.log("vao"+listitem);

        suri.find({"_id" : ObjectId(suri_id)})

            .then(post => {

                let p = post[0];
                p.status = status;
                p.description = description;

                if (listitem.length != 0) {
                    console.log("listitem pull"+listitem);
                    for (var i = 0; i <= (listitem.length - 1); i++) {
                        suri.findOneAndUpdate({_id: ObjectId(suri_id)}, {$pull: {images: listitem[i]}})
                            .then(result =>
                                resolve({ status: 200,status : result.status, description: result.description }))
                            .catch(err => reject({status: 500, message: "Internal Server Error !"}));
                    }
                }

                return p.save();

            })

            .then(result =>
                resolve({ status: 200,status : result.status, description: result.description }))

            .catch(err => {
                reject({status: 500, message: "Internal Server Error !"})
            })

    });

// post communiti- -> insert suri
exports.insertSuri_community = (userId, status, description, name, code,linkshow) =>

    new Promise((resolve, reject)=> {

        user.find({"_id" : ObjectId(userId)})

            .then(get_user_token =>{
                var token_main = get_user_token[0].token;

                const d = new Date();
                const timeStamp = d.getTime();
                const newSuri = new suri({
                    user_id                 : userId,
                    status                  : status,
                    description             : description,
                    linkshow                : linkshow,
                    name                    : name,
                    code                    : code,
                    view                    : 0,
                    type                    : "0",
                    number_comment          : 0,
                    list_token              : token_main,
                    list_user               : userId,
                    created_at              : timeStamp,
                    updated                  :timeStamp
                });
                newSuri.save()
                    .then(() =>{
                        const suri_in_notify = newSuri._id;

                        console.log("create suri community............ " + suri_in_notify + "  " + userId);
                        send_notification.push_messcomment(suri_in_notify,"","");
                        add_table_noti.addNotification(userId, suri_in_notify,"1",false)
                        // add_table_noti.addNotification(userId, suri_in_notify,"2",false)
                        // const newNotify = new notification({
                        //     suri_id             : suri_in_notify,
                        //     type_notification   : "Post",
                        //     number_user         : 0,
                        //     create_at           : timeStamp
                        // })
                        // newNotify.save()
                        //
                        //     .then(() => {
                        //         var lol = newNotify._id;
                        //
                        //         const de_Notify = new detail_notify({
                        //             notification_id     : lol,
                        //             user_id             : userId,
                        //             watching            : "0",
                        //             create_at           : timeStamp
                        //         });
                        //
                        //         de_Notify.save()
                         .then(() => {
                              resolve({status: 201, suri_id: suri_in_notify , response: 1, message: 'Suri Created Successfully !'});

                                    // })
                            })
                    })

                    .catch(err =>{
                        if(err.code == 11000){
                            reject({status: 409, message: 'Suri Already Registered !'});
                        } else{
                            reject({status: 500, message: 'Internal Server Error !'});
                        }
                    });
            })


    });

// Update a Suri in Community tab

exports.updateSuri_community = (suri_id, status, description, name, listitem) =>

    new Promise((resolve, reject) => {
        console.log(suri_id+" "+ status +" "+ description +" "+ name);

        suri.find({"_id" : ObjectId(suri_id)})

            .then(post => {
                let p = post[0];
                // const code_verify = p.code;
                console.log(p.user_id);
                if(p.user_id == "001001010101010101010111"){
                        console.log("Wtf");
                        let p = post[0];
                        p.status = status;
                        p.description = description;
                        p.name = name;
                        if (listitem.length != 0) {
                           for (var i = 0; i <= (listitem.length - 1); i++) {
                              console.log(suri_id + "/" + listitem[i]);
                              suri.findOneAndUpdate({_id: ObjectId(suri_id)}, {$pull: {images: listitem[i]}})
                                 .then(() => {
                                    resolve({status: 200, message: "Delete Image Success"});
                                 })
                                 .catch(err => reject({status: 500, message: "Internal Server Error !"}));
                           }
                        }

                        p.save()

                        .then(() => {
                            resolve({status: 200, message: "Update Suri Successfully !"})
                        })

                }else{
                        console.log("HIC");
                        let p = post[0];
                        p.status = status;
                        p.description = description;
                        p.name = name;
                        if (listitem.length != 0) {
                           for (var i = 0; i <= (listitem.length - 1); i++) {
                              console.log(suri_id + "/" + listitem[i]);
                              suri.findOneAndUpdate({_id: ObjectId(suri_id)}, {$pull: {images: listitem[i]}})
                                 .then(() => {
                                    resolve({status: 200, message: "Delete Image Success"});
                                 })
                                 .catch(err => reject({status: 500, message: "Internal Server Error !"}));
                           }
                        }

                        p.save()

                            .then(() => {
                                resolve({status: 200, message: "Update Suri Successfully !"})
                            })


                }

            })

            .catch(err => {
                reject({status: 500, message: "Internal Server Error !"})
            })

    });

exports.deleteSuri = (suri_id) =>

    new Promise((resolve, reject) => {
        console.log(suri_id);

        suri.find({"_id" : ObjectId(suri_id)})

            .then(su => {
                if(su.length === 0){
                    reject({status: 400, message: "Suri not found !"});
                }else{
                    return su[0];
                }
            })

            .then(s => {

                console.log(s.user_id);
                if(s.user_id == "001001010101010101010111"){

                            notification.remove({"suri_id" : ObjectId(suri_id)})

                                .then(() => {

                                    reply_comment.remove({"suri_id": ObjectId(suri_id)})

                                        .then(() => {

                                            comment.remove({"suri_id" : ObjectId(suri_id)})

                                                .then(() => {

                                                    s.remove()

                                                        .then(() => {
                                                            resolve({status:200, message: "Suri was deleted !"})
                                                        })
                                                })
                                        })
                                })


                }else{

                            notification.remove({"suri_id" : ObjectId(suri_id)})

                                .then(() => {
                                    reply_comment.remove({"suri_id": ObjectId(suri_id)})

                                        .then(() => {
                                            comment.remove({"suri_id" : ObjectId(suri_id)})

                                                .then(() => {
                                                    s.remove()

                                                        .then(() => {

                                                            resolve({status:200, message: "Suri was deleted !"})

                                                        })
                                                })
                                        })
                                })

                }
            })

            .catch(err => {
                reject({status:500, message: "Internal Server Error !"});
            })

    });


// insert suri
exports.uploadsuri = (suri_id, image) =>

    new Promise((resolve, reject) => {

        console.log(suri_id);

        // let ObjectId;
        // ObjectId = require("mongodb").ObjectID;

        suri.find({_id: ObjectId(suri_id)})
            .populate("suri")
            .then(suris => {

                if (suris.length === 0) {

                    reject({status: 404, message: "User Not Found !"});

                } else {

                    return suris[0];

                }
            })

            .then(suriPush => {
                suriPush.images.push(image);
                suriPush.save();

                resolve({status: 200,  message: "uploaded" });

            })
            .catch(err => reject({status: 500, message: "Internal Server Error !"}));

    });

// update view
exports.updateView = (suri_id, countView) =>

    new Promise((resolve, reject) => {

        console.log(suri_id);

        // let ObjectId;
        // ObjectId = require("mongodb").ObjectID;

        suri.find({_id: ObjectId(suri_id)})
            .populate("suri")
            .then(suris => {

                if (suris.length === 0) {

                    reject({status: 404, message: "User Not Found !"});

                } else {

                    return suris[0];

                }
            })

            .then(suriView => {
                suriView.view = countView;
                suriView.save();

                resolve({status: 200,  message: "uploaded" });

            })
            .catch(err => reject({status: 500, message: "Internal Server Error !"}));

    });