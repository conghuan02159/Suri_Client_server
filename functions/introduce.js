const table_user = require('../models/table_user');
const introduce = require('../models/table_introduce');
const notification = require('../functions/notifications');


exports.introduc_input = ( user_input, code_introduce) =>

    new Promise((resolve, reject) => {
        //reject({status: 404, message: password});
        table_user.find({ phone_number: code_introduce, state: "1", check_block:false})
            .then(users => {
                if(users.length ==0){
                    reject({status: 404, request_check:3, message: 'User Not Found !'});
                }else{
                    return users[0];
                }
            })

            .then(user => {

                const userFind = user._id;
                let ObjectId = require("mongodb").ObjectId;

                table_user.find({"_id": ObjectId(user_input)})
                    .then(userss => {
                        if (userss.length == 0) {
                            reject({status: 404, request_check: 0, message: 'User Not Found !'});
                        } else {
                            return userss[0];
                        }
                    })

                    .then(userInput => {
                        // kiểm tra thử đã nhập mã lần nào chưa
                        const check_input = userInput.check_input_code;

                        let countTimeCodeInput = userInput.time_code_introduce;
                        let moneyInput = userInput.money;

                        var d = new Date();
                        var timeStamp = d.getTime();



                        if (check_input == 1) {
                            resolve({status: 201, request_check: 0, code: code_introduce, message: 'code inputed one time'})

                        } else {

                            const newIntroduce = new introduce({

                                user_id: userFind,
                                user_input: user_input,

                                code_introduce: code_introduce,
                                check_pay: 0,
                                created_at: timeStamp

                            });
                            newIntroduce.save()
                                .then(() => {

                                    const iduser = newIntroduce.user_id;
                                  //  const user_input = newIntroduce.user_input;


                                    if (countTimeCodeInput != null) {
                                        countTimeCodeInput = countTimeCodeInput + 1;
                                        moneyInput = moneyInput + 5000;
                                    } else {
                                        countTimeCodeInput = 1;
                                        moneyInput = 25000;
                                    }

                                    table_user.update({"_id": ObjectId(user_input)}, {
                                        $set: {
                                            "time_code_introduce": countTimeCodeInput,
                                            "money": moneyInput
                                        }
                                    }).then(() => {


                                    //
                                    // let updateUser = user[0];
                                    let countTimeCode = user.time_code_introduce;
                                    let nmoney = user.money;


                                    if (countTimeCode != null) {
                                        countTimeCode = countTimeCode + 1;
                                        nmoney = nmoney + 5000;

                                    } else {
                                        countTimeCode = 1;
                                        nmoney = 25000;
                                    }

                                    console.log("count: " + countTimeCode + nmoney);

                                    table_user.update({"_id": ObjectId(iduser)}, {
                                        $set: {
                                            "time_code_introduce": countTimeCode,
                                            "money": nmoney
                                        }
                                    })
                                        .then(() => {
                                            table_user.update({"_id": ObjectId(user_input)}, {$set: {"check_input_code": 1}})
                                                .then(() => {
                                                    notification.notification_send_introduce(userFind, user_input)
                                                        .then(() => {
                                                                resolve({
                                                                    status: 201,
                                                                    request_check: 1,
                                                                    userid: iduser,
                                                                    phone: code_introduce,
                                                                    message: 'user Registered Successfully'
                                                                });
                                                            }
                                                        );
                                                })

                                        });
                                })


                                })


                                .catch(err => {
                                    if (err.code == 11000) {
                                        reject({status: 409, request_check: 1, message: 'User Already Registered !'});

                                    } else {
                                        reject({status: 500, request_check: 2, message: 'Internal Server Error !'});
                                    }
                                });
                        }
                        // userSuri: table_introduce connect with table_user


                    })
                    .catch(err => reject({status: 500, request_check: 2, message: 'Internal Server Error !'}));
            })
    });


// infomation pay
exports.infomation_pay = (id_user) =>

    new Promise((resolve, reject) => {
        //reject({status: 404, message: password});
        table_user.find({_id: id_user})
            .then(users => {
                if(users.length ==0){
                    reject({status: 404, request_check:0, message: 'User Not Found !'});
                }else{
                    return users[0];
                }
            })

            .then(user => {


                    const time_code_introduce = user.time_code_introduce;
                    const  money = user.money;


                    resolve({status: 200, time_code_introduce: time_code_introduce, money:  money });

            })
            .catch(err => reject({ status: 500,request_check: 2, message: 'Internal Server Error !'}));
    });
