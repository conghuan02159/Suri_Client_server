'use strict';

const comment =new require('../models/table_comment');
const user = require('../models/table_user');
const suri =new require('../models/table_suri');
const rep_comment = require('../models/table_reply_comment');
const  category = require('../models/table_category');

const send_notification = require('../functions/notification_send');
const add_table_noti = require('../functions/add_table_notification');

let ObjectId = require("mongodb").ObjectId;

exports.commentDetail = (comment_id,skip) =>
	new Promise((resolve, reject) => {
		console.log(comment_id);
		comment.find({"_id" : ObjectId(comment_id)})
		.populate({path : "rep_comment", select : "user_id status name create_at code_reply",
			options: {
				sort : {"create_at" : -1},
				limit : 10,
				skip : (10*skip)

			}
		})

		.then(comments => {
			if(comments.length === 0){
				reject({status: 404, message: "Bình luận không tồn tại !"});
			}else{
				return comments[0];
			}
		})

		.then(comments_one => {
			resolve(comments_one);
		})

		.catch(err => reject({status: 500, message: "Lỗi Server"}));

	});



exports.addComment = (user_id, suri_id, status, code_comment, name ) =>

    new Promise((resolve, reject) => {
        // let number_cmt;
        // comment.count({"suri_id" : ObjectId(suri_id)}, function(error, count){
        //     number_cmt = count;
        // })

            // .then(() => {
                const d = new Date();
                const timeStamp = d.getTime();
                const newComment = new comment({
                    user_id     : user_id,
                    suri_id     : suri_id,
                    status      : status,
                    code_comment: code_comment,
                    name        : name,
                    create_at   : timeStamp

                });
                newComment.save()

                    .then(() => {

                        add_table_noti.addNotificationComment(user_id,name, suri_id,"1",true);
                        console.log(" add comment success  ");

                        comment.count({"suri_id" : ObjectId(suri_id)}, function (error, count) {


                            const df = new Date();
                            const timeStampa = df.getTime();
                            console.log("count .  ................. " + count+ "   " + suri_id + " ... "+timeStampa);
                            suri.updateOne({"_id": ObjectId(suri_id)},
                                {$set: {"number_comment":count, "updated":timeStampa}},function (err, result) {
                                    if (err) {
                                        console.log('Error updating notification object: ' + err);
                                    }else{
                                        console.log('rrrrrrrrrrrrrrrrrrrrrrr' + result.toString() + ' updated time admin ');
                                    }
                                });

                        });
                   //      notification.findOne({"_id" : ObjectId(suri_id)})
                   //          .then(result1 => {
                   //   if(result1 != null){
                   //
					// 		console.log(" du lieu bang thong bao != null  " + result1)
                   //
                   //       adddetail_noti(result1._id, user_id);
                   //
                   //   }else{
                   //
                   //       addNotification(user_id, suri_id,"1");
                   //
                   //
                   //       console.log(" du lieu bang thong bao == null " + result1)
                   //
                   //       // user.find({"_id" : ObjectId(user_id)}).then(result2 => { })
                   //
                   //   }
                   // })

                    })
                    .then(() => {
                        let comment_id = newComment._id;
                        let user_id = newComment.user_id;
                        let suri_id = newComment.suri_id;
                        let status = newComment.status;
                        let code_comment = newComment.code_comment;
                        let name = newComment.name;
                        let create_at = newComment.create_at;


                        suri.find({"_id" : ObjectId(suri_id)})
                            .then(tbsuris =>{
                              let title = tbsuris[0].status;
                              let cate = tbsuris[0].category;
                              let type = tbsuris[0].type;


                              if(type === "1"){
                                  category.find({"_id": ObjectId(cate)})

                                      .then(categorys => {
                                          let count_Comment = categorys[0];
                                          var number = count_Comment.count_comment;

                                          if (number != 0) {
                                              number = number + 1;

                                          }
                                          else{
                                              number = 1;
                                          }
                                          console.log("yyyyyyyyyyyyy "+number);
                                          count_Comment.count_comment = number;
                                          count_Comment.save()
											  .then(()=> {
                                                  user.find({"_id" : ObjectId(user_id)})
                                                      .then(tbusers =>{
                                                          let  avatar = tbusers[0].image;
                                                          const messaged = { type_notification:"comment"};
                                                          console.log("comment");
                                                          var infor = {comment_id: comment_id, user_id: user_id, suri_id: suri_id ,
                                                              status  : status ,code_comment:code_comment, avatar: avatar, name:name, title: title, create_at:create_at};
                                                          send_notification.push_messcomment(suri_id,messaged,infor);
                                                          resolve({
                                                              warning: 200,
                                                              comment_id : comment_id,
                                                              user_id : user_id,
                                                              suri_id : suri_id,
                                                              code_comment : code_comment,
                                                              status : status,
                                                              name : name,
                                                              create_at : create_at
                                                              // message: "Comment created suscessfully !"
                                                          });


                                                      });
                                              });


                                      });

                              }else{
                                  user.find({"_id" : ObjectId(user_id)})
                                      .then(tbusers =>{
                                          let  avatar = tbusers[0].image;
                                          const messaged = { type_notification:"comment"};
                                          console.log("comment");
                                          var infor = {comment_id: comment_id, user_id: user_id, suri_id: suri_id ,
                                              status  : status ,code_comment:code_comment, avatar: avatar, name:name, title: title, create_at:create_at};
                                          send_notification.push_messcomment(suri_id,messaged,infor);
                                          resolve({
                                              warning: 200,
                                              comment_id : comment_id,
                                              user_id : user_id,
                                              suri_id : suri_id,
                                              code_comment : code_comment,
                                              status : status,
                                              name : name,
                                              create_at : create_at
                                              // message: "Comment created suscessfully !"
                                          });


                                      });
							  }




                            });


                    })


                    .catch(err => reject({status: 500, message: "Internal Server Error !"}));
            // })

    });

exports.updateComment = (comment_id, code_comment, status) => 
	new Promise ((resolve, reject) => {

		let ObjectId = require("mongodb").ObjectId;

		comment.find({
			"_id" : ObjectId(comment_id)
		})

		.then(comments => {
			if(comments.length === 0){
				reject({status: 404, message: "Không tìm thấy bình luận"});
			}else{
				return comments[0];
			}
		})
		
		.then(comment_one => {
			const code = comment_one.code_comment;
			const id_cmt = comment_one._id;
			if(code_comment === code){
				comment.update({"_id": ObjectId(id_cmt)}, {$set: {"status": status}})

				.then(() => {
					resolve({status: 200, message: "Cập nhật thành công !"});
				})
			}else{
				reject({status: 401, message: "Sai mã code"});
			}
		})

		.catch(err => reject({status: 500, message: "Lỗi Server !"}));
	});


exports.deleteComment = (comment_id, code_comment, suri_id,user_id) =>

	new Promise ((resolve, reject) => {
		let ObjectId = require("mongodb").ObjectId;
		let number_cmt;

		comment.count({"suri_id" : ObjectId(suri_id)}, function(error, count){
			number_cmt = count;
		})

		.then(() => {
			comment.find({
				"_id" : ObjectId(comment_id)
			})

			.then(co => {
				if(co.length === 0){
					reject({status: 404, message: "Không tìm thấy bình luận !"});
				}else{
					return co[0];	
				}
			})

			.then(c => {
				var code = c.code_comment;
				var id_cmt = c._id;
				if(c.user_id == "001001010101010101010111"){

					if(code_comment === code){
						user_id = c.user_id;
						suri.findByIdAndUpdate(
							suri_id,
							{$set: {"number_comment" : number_cmt - 1}},
							{safe: true, upsert: true, new: true},
							function(err, model){}
						);

						rep_comment.remove({"comment_id" : ObjectId(id_cmt)})

						.then(() => {

							c.remove()

							.then(() => {
                                console.log("delete1 " + suri_id);
                                add_table_noti.deleteNotification(comment_id,user_id, suri_id, "1");

								resolve({status: 200, message: "Đã xóa !" });
							})
						})

					}else{

						reject({status: 401, message: 'Sai mã code !'});

					}
				}else{
					if(code_comment === "0"){
						suri.findByIdAndUpdate(suri_id, {$set: {"number_comment" : number_cmt - 1}},
							{safe: true, upsert: true, new: true},
							function(err, model){}
						);
						rep_comment.remove({"comment_id" : ObjectId(id_cmt)})
						.then(() => {
							c.remove()
							.then(() => {
                                console.log("delete2 " + suri_id);
                                add_table_noti.deleteNotification(comment_id,user_id, suri_id, "1");
								resolve({status: 200, message: "Đã xóa !" });
							})
						})
					}else{
						reject({status: 401, message: 'Sai mã code !'});

					}
				}

			})

			
		})
		.catch(err => reject({status: 500, message: "Lỗi Server !"}));

	});