'use strict';

const FCM = require("fcm-node");
const fcm = new FCM("AIzaSyCcMhiOmvyUOCf2Dy0JHYslGi1OM2ZpWoQ");

exports.push_messtotopic = (token,messagetype, infor) =>
    new Promise((resolve, reject) => {
            // token = ["9K-guv1A1Y:APA91bGIVQ13nFsPZyGEZOLjvs4RRN7S4R17t3BYWo0s8fSHsq_bB2-93c2UsqQSq9rBgLMg0F0AIZjRLRmx8giQ69aJh8JpQNNKEPx38zQ0OUh9dOt8KDEKv2_PhflOnP3f11uuOj-2"];
        const message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
            to:token,
            data: {
                infor : infor,
                messagetypes : messagetype
            }
        };
        console.log("send .................... : " + token);
        fcm.send(message, function (err, response) {
            if (err) {
                console.log("ffffffffff " + err.toString());
                reject({status: 409, message: "MessToTopic Error !" + err.toString()});
            } else {
                console.log(response);
                resolve({status: 201, message: "MessToTopic Sucessfully !", response: response});
            }
        });
    });


exports.push_messcomment = (suri_id,messagetype, infor) =>
    new Promise((resolve, reject) => {
        // token = ["9K-guv1A1Y:APA91bGIVQ13nFsPZyGEZOLjvs4RRN7S4R17t3BYWo0s8fSHsq_bB2-93c2UsqQSq9rBgLMg0F0AIZjRLRmx8giQ69aJh8JpQNNKEPx38zQ0OUh9dOt8KDEKv2_PhflOnP3f11uuOj-2"];
        const message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
            to:"/topics/" + suri_id,
            data: {
                infor : infor,
                messagetypes : messagetype
            }
        };
        // console.log("push mess: " + infor , " messagetype " + messagetype);
        fcm.send(message, function (err, response) {
            if (err) {
                // console.log(err);
                reject({status: 409, message: "MessToTopic Error !"});
            } else {
                console.log(response);
                resolve({status: 201, message: "MessToTopic Sucessfully !", response: response});
            }
        });
    });




