'use strict';

const suritb = require('../models/table_suri');



exports.HomePostPublic = (user_id, status, image, description, code_suri, name, type) =>
	new Promise((resolve,reject) => {
        var d = new Date();
        var timeStamp = d.getTime();
        console.log(timeStamp);
		const newpost = new suritb({
            user_id: user_id,
            status: status,
            image: image,
            description:description,
            code_suri:code_suri,
            name:name,
            type:type,
			created_at: timeStamp
		});
        newpost.save()
		.then(() =>{
            //const  user = newpost[0];
            const  created_at = newpost.created_at;
            resolve({ status: 201,user_id: created_at,  message: 'Post new public suri Sucessfully !' })
        } )
		.catch(err => reject({ status: 500, message: 'Error add not Sucessfully !' }))
			// if (err.code == 11000) {
			// 	reject({ status: 409, message: 'User Already Registered !' });
            //
			// } else {
        // }
	});