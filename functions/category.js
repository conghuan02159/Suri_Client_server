const category = require('../models/table_category');
let ObjectId = require("mongodb").ObjectId;

exports.updateCountCategory =(category_id, name, count_post, count_comment) =>
    new Promise((resolve, reject) => {


        category.find({"_id": ObjectId(category_id)})
            .then(categorys => {
                const d = new Date();
                const timeStamp = d.getTime();
                if(categorys.length == 0){
                    // category: name connect with table category
                    const newCategory= new category({

                        name_category: name,
                        count_comment: count_comment,
                        count_post: count_post,
                        created_at: timeStamp

                    });
                    newCategory.save()

                        .then(() => {

                            resolve({status: 200, category: category,  count_post: count_post, count_comment: count_comment, message: 'user Registered Successfully'})
                        })

                        .catch(err =>{
                            if(err.code == 11000){
                                reject({status: 409, request_check: 1, message: 'User Already Registered !'});

                            }else{
                                reject({status: 500, request_check: 2, message: 'Internal Server Error !'});
                            }
                        });

                }else{

                    let category = categorys[0];
                    if(count_comment !=0){
                        category.count_comment = count_comment;
                    }else if(count_post !=0){
                        category.count_post = count_post;
                    }
                    category.save()
                        .then(() => {
                            resolve({status: 200, category_id: category_id,  count_post: count_post, count_comment: count_comment, message: 'user Registered Successfully'})
                        })

                        .catch(err =>{
                            if(err.code == 11000){
                                reject({status: 409, request_check: 1, message: 'User Already Registered !'});

                            }else{
                                reject({status: 500, request_check: 2, message: 'Internal Server Error !'});
                            }
                        });


                }
            })
            .catch(err =>{
                reject({status: 500, request_check: 2, message: 'Internal Server Error !'});
            });

    });

exports.show_category = () =>
    new Promise((resolve, reject) => {

        //  suri.find({"type" : "1", "category": category},{ "__v" : 0}).sort({"_id" : -1})
        category.find().sort({"_id" : -1})

            .then(categorys => {
                resolve({ status: 200, category: categorys,  message: ' suri repair Sucessfully !' })})

            .catch(err => reject({status: 500, message: "Internal Server Error !"}));

    });

exports.registerCategory =(category_id, name, count_post, count_comment) =>
    new Promise((resolve, reject) => {

        // kiem tra phone da duoc su dung hay chua
        category.find()
            .then(categorys => {
                const d = new Date();
                const timeStamp = d.getTime();

                    // category: name connect with table category
                    const newCategory= new category({

                        name_category: name,
                        count_comment: count_comment,
                        count_post: count_post,
                        created_at: timeStamp

                    });
                    newCategory.save()

                        .then(() => {

                            resolve({status: 200, category: category,  count_post: count_post, count_comment: count_comment, message: 'user Registered Successfully'})
                        })

                        .catch(err =>{
                            if(err.code == 11000){
                                reject({status: 409, request_check: 1, message: 'User Already Registered !'});

                            }else{
                                reject({status: 500, request_check: 2, message: 'Internal Server Error !'});
                            }
                        });


            })
            .catch(err =>{
                reject({status: 500, request_check: 2, message: 'Internal Server Error !'});
            });

    });
