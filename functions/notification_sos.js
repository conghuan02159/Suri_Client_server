'use strict';

const table_user = require('../models/table_user');
const send_notification = require('../functions/notification_send');

exports.notification_send_driver = (id_customer, id_driver) =>
    new Promise((resolve, reject) => {
        // console.log( + " id_customer " + id_customer + " id_driver " +id_driver)
        let ObjectId = require("mongodb").ObjectId;
        table_user.find({"_id" : ObjectId(id_driver)})
            .then(id_driver => {
                if(id_driver.length === 0){
                    reject({status: 404, message: "không tồn tại !"});
                }else{
                    return id_driver[0];
                }
            })

            .then(id_driver => {
                table_user.find({"_id": ObjectId(id_customer)})
                    .then(id_customer => {
                        if (id_customer.length === 0) {
                            reject({status: 404, message: "không tồn tại !"});
                        } else {
                            return id_customer[0];
                        }
                    }).then(customer => {
                    const name = customer.name;
                    const phone_number = customer.phone_number;
                    const token = id_driver.token;

                    var message =  { type_notification: "sos_driver" };
                    var infor = {name : name, phone_number : phone_number, id_customer : id_customer};
                    console.log(" token driver ---- " + token + " infor " + infor + " name " + name + " phone_number "
                        + phone_number +" idcustomer: "+id_customer + " message " + message);
                    send_notification.push_messtotopic(token,message,infor);


                    resolve({status: 200, token: token, messagetype : message, infor : infor});
                }).catch(err => reject({status: 500, message: "Lỗi Server"}))
            })
            .catch(err => reject({status: 500, message: "Lỗi Server"}));
    });

exports.notification_send_customer = (id_customer, booleansdri,id_driver) =>
    new Promise((resolve, reject) => {
        // console.log( + " id_customer " + id_customer + " id_driver " +id_driver)
        let ObjectId = require("mongodb").ObjectId;
        table_user.find({"_id" : ObjectId(id_customer)})
            .then(customer => {
                if(customer.length === 0){
                    reject({status: 404, message: "không tồn tại !"});
                }else{
                    return customer[0];
                }
            })
            .then(customer => {
                table_user.find({"_id": ObjectId(id_driver)})
                    .then(driver => {
                        if (driver.length === 0) {
                            reject({status: 404, message: "không tồn tại !"});
                        } else {
                            return driver[0];
                        }
                    }).then(driver => {
                    const token = customer.token;
                    const name = driver.name;
                    const phone_number = driver.phone_number;
                    const message = { type_notification:"sos_customer"};
                    var infor = {name: name, phone_number: phone_number, id_driver: id_driver , resultdri  : booleansdri};
                    // console.log(" token driver ---- " + token + " infor " + infor + " name " + name + " phone_number " + phone_number);
                    send_notification.push_messtotopic(token,message,infor);
                    resolve({status: 200, token: token, messsagetype : message, infor: infor});
                }).catch(err => reject({status: 500, message: "Lỗi Server"}))
            })
            .catch(err => reject({status: 500, message: "Lỗi Server"}));
    });

// remove from driver
exports.notification_remove_from_driver = (id_customer, id_driver, reason) =>
    new Promise((resolve, reject) => {
        // console.log( + " id_customer " + id_customer + " id_driver " +id_driver)
        let ObjectId = require("mongodb").ObjectId;
        table_user.find({"_id" : ObjectId(id_customer)})
            .then(customer => {
                if(customer.length === 0){
                    reject({status: 404, message: "không tồn tại !"});
                }else{
                    return customer[0];
                }
            })
            .then(customer => {
                table_user.find({"_id": ObjectId(id_driver)})
                    .then(driver => {
                        if (driver.length === 0) {
                            reject({status: 404, message: "không tồn tại !"});
                        } else {
                            return driver[0];
                        }
                    }).then(driver => {
                    const token = customer.token;
                    const name = driver.name;
                    const phone_number = driver.phone_number;
                    const message = { type_notification:"sos_remove_from_driver"};
                    var infor = {name: name, phone_number: phone_number, id_driver: id_driver, reason: reason};
                    // console.log(" token driver ---- " + token + " infor " + infor + " name " + name + " phone_number " + phone_number);
                    send_notification.push_messtotopic(token,message,infor);
                    resolve({status: 200, token: token, messsagetype : message, infor: infor});
                }).catch(err => reject({status: 500, message: "Lỗi Server"}))
            })
            .catch(err => reject({status: 500, message: "Lỗi Server"}));
    });