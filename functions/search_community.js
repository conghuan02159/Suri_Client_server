const suri = require('../models/table_suri');
const user = require('../models/table_user');
const bcrypt = require('bcryptjs');

exports.cSearch = (keysearch, page) =>
    new Promise((resolve,reject) => {
        const limit = 10;
        if(page.is)
            if(page<1) page = 1;
        const start = (limit * page) - limit;
        console.log(keysearch);
        suri.find(
            {$and: [{'type' : "0"},
                {$or:[ {'status': {  $regex :  keysearch  }},{'description': {  $regex :  keysearch  }}]}]}
        )

            .populate({path: "user_id", select: "_id name image"})
            .skip(start).limit(limit)
            .sort({created_at: -1})

            .then(product => {
                resolve({status: 200, message: product});

            })
            .catch(err => reject({status: 500, message: "Internal Server Error !"}));
    });