const callSos = require('../models/table_statistic');


exports.statisticSos = ( countSos, countScan, countMessage) =>

    new Promise((resolve,reject) => {

        var calendar = new Date();
        calendar.setHours(0);
        calendar.setMinutes(0);
        calendar.setSeconds(0);
        calendar.setMilliseconds(0);


var timeStamp = calendar.getTime();

        var strDated = calendar;
        callSos.find({created_at: timeStamp})

            .then(callsoss => {

                if(callsoss.length ==0){

                    const newCallSos = new callSos({

                        count_call_sos: countSos,
                        count_scan: countScan,
                        count_message: countMessage,
                        created_at: timeStamp,
                        date_created: strDated
                    });
                    newCallSos.save()

                        .then(() => {



                            resolve({status: 201,  count_sos: countSos, created_at: timeStamp,    message: 'Count call sos'})
                        })

                        .catch(err =>{
                            if(err.code == 11000){
                                reject({status: 409, request_check: 1, message: 'User Already Registered !'});

                            }else{
                                reject({status: 500, request_check: 2, message: 'Internal Server Error !'});
                            }
                        });

                }
                else{

                    if(countSos != 0){
                        let count_sos = callsoss[0];
                        let countCallSos = count_sos.count_call_sos;

                        if (countCallSos != null) {
                            countCallSos = countCallSos + 1;

                        }
                        else{
                            countCallSos = 1;
                        }
                        var timeC = count_sos.created_at;

                        var date = new Date();
                        date.setTime(timeC);
                        var strDate = date;
                        count_sos.date_created = strDate;


                        console.log("yyyyyyyyyyyyy "+countCallSos);
                        count_sos.count_call_sos = countCallSos;
                        count_sos.save()

                            .then(() => {

                                resolve({status: 201,    count_sos: countCallSos, created_at: timeC,
                                    message: 'user Registered Successfully'});
                            }) ;

                    }else if(countScan !=0){
                        let count_sos = callsoss[0];
                        let countScan = count_sos.count_scan;

                        if (countScan != null) {
                            countScan = countScan + 1;

                        }else{
                            countScan = 1;
                        }
                        var timeC = count_sos.created_at;

                        var date = new Date();
                        date.setTime(timeC);
                        var strDate = date;
                        count_sos.date_created = strDate;

                        console.log("yyyyyyyyyyyyy "+countScan);
                        count_sos.count_scan = countScan;
                        count_sos.save()

                            .then(() => {

                                resolve({status: 201,    count_sos: countScan, created_at: timeC,
                                    message: 'user Registered Successfully'});
                            }) ;
                    }else if(countMessage !=0){
                        let count_sos = callsoss[0];
                        let countMessage = count_sos.count_message;

                        if (countMessage != null) {
                            countMessage = countMessage + 1;

                        }else {
                            countMessage =1;
                        }
                        var timeC = count_sos.created_at;

                        var date = new Date();
                        date.setTime(timeC);
                        var strDate = date;
                        count_sos.date_created = strDate;


                        console.log("yyyyyyyyyyyyy "+countMessage);
                        count_sos.count_message = countMessage;
                        count_sos.save()

                            .then(() => {

                                resolve({status: 201,    count_sos: countMessage, created_at: timeC,
                                    message: 'user Registered Successfully'});
                            }) ;
                    }




                }

            }).catch(err => {

            reject({status: 500, message: 'Internal Server Error !'});
        })
    });