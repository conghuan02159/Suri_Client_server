'use strict';

const suri = require('../models/table_suri');
const user = require('../models/table_user');
const comment = require('../models/table_comment');
let ObjectId  = require("mongodb").ObjectId;

exports.showSuri_community = (skip) =>
    new Promise((resolve, reject) => {

        suri.find({"type" : "0"},{"__v" : 0}).sort({"updated" : -1})

            .limit(10)

            .skip(10 * skip)

            .populate({path: "user_id", select: "name"})

            .then(suri => resolve(suri))

            .catch(err => reject({status: 500, message: "Internal Server Error !"}));
    });

exports.showSuri_repair = (skip, category) =>
    new Promise((resolve, reject) => {

       suri.find({"type" : "1", "category": ObjectId(category)},{ "__v" : 0}).sort({"_id" : -1})
        // suri.find({"type" : "1"},{ "__v" : 0}).sort({"_id" : -1})
            .limit(10)

            .skip(10 * skip)

            .populate({path: "user_id", select: "name"})

            .then(suri => {
                resolve({ status: 200, surihomess: suri,  message: ' suri repair Sucessfully !' })})

            .catch(err => reject({status: 500, message: "Internal Server Error !"}));

    });


exports.showSuri = (suri_id) =>
    new Promise((resolve, reject) => {


        // console.log(suri_id);

        // suri.find({'_id' : ObjectId(suri_id)}, {"_id" : 0, "temp_password" : 0, "temp_password_time" : 0})
        suri.find({'_id' : ObjectId(suri_id)}, {"_id" : 0,"list_token" : 0,"list_user" : 0,"__v" : 0})

            .populate({
                path: "user_id", select: "_id name phone_number"
            })

            .then(suris => {
                if(suris.length === 0){
                    reject({status : 404, message: "Suri not found !"});
                }else{
                    return suris[0];
                }
            })

            .then(suri => resolve(suri))

            .catch(err => reject({status: 500, message: "Internal Server Error !"}));

    });

exports.show_forum_repair = (category,skip) =>
    new Promise((resolve, reject) => {

        suri.find({"category": ObjectId(category)},{ "__v" : 0,"list_token" : 0 , "list_user":0}).sort({"_id" : -1})
        // suri.find({"type" : "1"},{ "__v" : 0}).sort({"_id" : -1})
            .limit(10)

            .skip(10 * skip)

            .populate({path: "user_id", select: "name"})

            .then(suri => {
                resolve({ status: 200, feedForum: suri,  message: ' suri repair Sucessfully !' })})

            .catch(reject => reject({status: 500, message: "Internal Server Error !"}));

    });

exports.showComment_InSuri = (suri_id, skip) =>
    new Promise ((resolve, reject) => {



        comment.find({"suri_id" : ObjectId(suri_id)}).sort({"create_at" : -1})

            .populate({
                path: "rep_comment", select: "_id name status create_at user_id", options : {sort : {"create_at" : 1}}
                //options : {sort : {"create_at" : 1}}, populate : {path:"user_id",select: "image"}
            })

            .limit(10)

            .skip((10*skip))

            .then(get_comments => {
                if(get_comments.length === 0){
                    reject({status: 404, message: "Không tìm thấy dữ liệu"});
                }else{
                    return get_comments;
                }
            })

            .then(get_comment => resolve(get_comment))

            .catch(err => reject({status: 500, message: "Lỗi Server !"}))

    });

// count forum


