const user = require('../models/table_user');
const bcrypt = require('bcryptjs');

exports.login_user = (phonenumber, password) =>

	new Promise((resolve, reject) => {
		//reject({status: 404, message: password});
		user.find({ phone_number: phonenumber})
		.then(users => {
			if(users.length ==0){
				reject({status: 404, request_check:0, message: 'User Not Found !'});
			}else{
				return users[0];
			}
		})


		.then(user => {
			const hashed_password = user.hashed_password;
			if(bcrypt.compareSync(password, hashed_password)){
				const name = user.name;
				const iduser = user._id;
				const  state = user.state;
				const user_type = user.user_type;
				const job = user.fix_job;
				const avatar = user.image;
				const checkBlock = user.check_block;
				resolve({status: 200, phone: phonenumber, name:name, userid:iduser, state: state, user_type: user_type, job: job, avatar: avatar, check_block: checkBlock });

			}else{
				reject({status: 401, request_check: 0, message: 'Invalid Credentials !'});
			}
		})
		.catch(err => reject({ status: 500,request_check: 2, message: 'Internal Server Error !'}));
});

	// login with facebook

	exports.loginFaceBook = (faceId, name,  lattitude, longitude,userType, token,DevIDShort_check,AndroidID_check,szImei_check,version) =>

	new Promise((resolve,reject) => {

            user.find({face_id: faceId})
                .then(users => {

                    if(users.length ==0){

                        userSuri.find({ "$or": [{
                                "DevIDShort_check" : DevIDShort_check
                            }, {
                                "AndroidID_check" : AndroidID_check
                            },{
                                "szImei_check" : szImei_check
                            }]
                        },{"DevIDShort_check":1,"AndroidID_check":1,"szImei_check":1},function (err, result) {
                            if (err) {
                                console.log('Error updating notification object: ' + err);
                            }else{
                                if(result.length>2){
                                    console.log('...................check trung qua nhieu may ' + result.length);
                                    reject({status: 410, request_check: 4, message: 'Dang ky qua nhieu user facebook !'});

                                }else{
                                    var d = new Date();
                                    var timeStamp = d.getTime();
                                    const newUser = new user({
                                        face_id: faceId,
                                        name: name,
                                        image: "SuriAvatar.png",
                                        token	  : token,
                                        user_type : userType,
                                        location : {
                                            type : "Point",
                                            coordinates : [
                                                longitude,lattitude
                                            ]
                                        },
                                        state		: "0",
                                        created_at: timeStamp,
                                        DevIDShort_check: DevIDShort_check,
                                        AndroidID_check:AndroidID_check,
                                        szImei_check:szImei_check,
                                        check_block: false,
                                        version : version,
                                        money : 20000
                                    });
                                    newUser.save()
                                        .then(() => {
                                            const iduser  = newUser._id;
                                            const phone = newUser.phone_number;
                                            var  checkBlock = newUser.check_block;
                                            resolve({status: 201,  userid: iduser, name: name,  userType: userType,
                                                state: "0", phone:phone,  check_block: checkBlock,  message: 'user Registered Successfully'})
                                        })

                                        .catch(err =>{
                                            if(err.code == 11000){
                                                reject({status: 409, request_check: 1, message: 'User Already Registered !'});

                                            }else{
                                                reject({status: 500, request_check: 2, message: 'Internal Server Error !'});
                                            }
                                        });
                                }
                            }

                        });
                    }
                    else{
                        const name = users[0].name;
                        const user_id = users[0]._id;
                        const userType = users[0].user_type;
                        const phone = users[0].phone_number;
                        const state = users[0].state;
                        const  checkBlock = user[0].check_block;
                        resolve({status: 201,  userid: user_id, name: name,  userType: userType,
                            phone: phone, check_block: checkBlock, state: state, message: 'Login Successfully'});
                    }
                })
        .catch(err => {
        reject({status: 500, message: 'Internal Server Error !'});
        });
	});

	// update avatar facebook
exports.updateAvatarFacebook = (userId, imageLink) =>

    new Promise((resolve,reject) => {
        user.find({_id: userId})
            .then(users => {

                if(users.length !=0){

                    let user = users[0];
                    user.image = imageLink;
                    user.save()

                        .then(() => {

                            resolve({status: 201,  userid: userId,
                                  message: 'user Registered Successfully'})
                        })
                        .catch(err =>{
                            if(err.code == 11000){
                                reject({status: 409, request_check: 1, message: 'User Already Registered !'});

                            }else{
                                reject({status: 500, request_check: 2, message: 'Internal Server Error !'});
                            }
                        });

                }else{
                    reject({status: 409, request_check: 1, message: 'User Already Registered !'});
				}


            }).catch(err => {

            reject({status: 500, message: 'Internal Server Error !'});
        })
    });