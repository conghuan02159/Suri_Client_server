'use strict';
const tbSuri = require('../models/table_suri');
// const ObjectId = require("mongodb").ObjectID;
const FCM = require("fcm-node");
const fcm = new FCM("AIzaSyCfOzn4FLIlITpNqOxq7HIclXzcrH5qw58");

// exports.getHomeSuri =() =>
//     new Promise((resolve,reject) => {
//         // user.find({ email: email }, { name: 1, email: 1, created_at: 1, _id: 0 })
//         tbSuri.find()
//             // .Object.keys("user_id").length
//             .sort({
//                 "created_at" : -1
//             })
//             .then(Suris => {
//                         this.push_messtotopic("eiz9CCnlgss:APA91bHDxeTQVoAjflyl5-hU2pwgTOdeL4KLWwUH2Fk6dEnVvCEyGJHGr7x9HXv_QFWX_5CyrhHS76DAs4w1HP8hs7CEc6NrqgjmY9kDb1FnT2qs__fEbBI9dka6eGYV20Hx-W_Vi1Lm"
//                             ,Suris);
//                 resolve({ status: 200,homesuris: Suris,  message: 'Post new public suri Sucessfully !' })})
//             .catch(err => reject({ status: 500, message: 'Internal Server Error !' }))
//     });


exports.push_messtotopic = (token, userid) =>
    new Promise((resolve, reject) => {
        const message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
            to: token,
            data: {
                iduser : userid
            }
        };
        console.log("push mess: " +userid ,"push token: " +token  );

        fcm.send(message, function (err, response) {
            if (err) {
                console.log(err);
                reject({status: 409, message: "MessToTopic Error !"});
            } else {
                console.log(response);
                resolve({status: 201, message: "MessToTopic Sucessfully !", response: response});
            }
        });
    });