/**
 * Created by DELL on 8/14/2017.
 */
const updateVersion = require('../models/table_update_version');

exports.CheckVersion = () =>

    new Promise((resolve, reject) => {

        updateVersion.find({})
        //

            .then((versions) => {

                if (versions.length == 0) {
                    var d = new Date();
                    var timeStamp = d.getTime();
                    const newCreateVersion = new updateVersion({

                        version: "1.1.8",
                        created_at: timeStamp
                    });

                    newCreateVersion.save()
                        .then(() => {
                            resolve({status: 200, message: 'Save Successfully'})
                        })
                        .catch(err => {
                            reject({status: 500, message: 'Internal Server Error !'});
                        });
                } else {
                    const versionRes = versions[0].version;
                    resolve({status: 200, version: versionRes, message: 'Save Successfully'})
                }
            })


            .catch(err => {
                if (err.code == 11000) {
                    reject({status: 409, message: 'User Already Registered !'});

                } else {
                    reject({status: 500, message: 'Internal Server Error !'});
                }
            });


    });