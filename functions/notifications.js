'use strict';
const countNofication = require('../models/table_count_notification');


const tbnotification = new require('../models/table_notification');

const tbUser = require('../models/table_user');
const send_notification = require('../functions/notification_send');
let ObjectId = require("mongodb").ObjectId;


exports.view_notifications = (user_sender,suri_id,type_notification) =>
    new Promise ((resolve, reject) => {
        console.log('' + user_sender+ ' ----- ' + suri_id + " ---  " + type_notification);
        tbnotification.updateOne({"user_sender" : ObjectId(user_sender),"suri_id" : ObjectId(suri_id) ,"type_notification" : type_notification, "interactive" : true},
            {$set: {"watching":"1"}},function (err, result) {
                if (err) {
                    console.log('Error updating notification object: ' + err);
                    // reject({status: 404, message: "Not Found update!"})
                }else{
                    console.log('' + result.toString() + ' updated view');
                    // resolve({status: 200, message: "1 user !"});
                }
            })
    });
// History notification suri
exports.historynotification = (user_id,page) =>

    new Promise((resolve, reject) => {

        let limit = 10;
        if (page.is)
            if (page < 1) page = 1;
        let start = (limit * page);

        tbnotification.find({$or: [{"user_sender": ObjectId(user_id),"number_user":{$gt:1},"interactive":true},
                {"user_create" : ObjectId(user_id),"user_sender" : ObjectId(user_id),"number_user":{$gt:0},"interactive" : false},
                {"type_notification" : "3"}]})
            .populate({path: "suri_id" ,select: "status name images"})
            .populate({path: "last_person" ,select: "name image"})
            .populate({path: "user_create" ,select: "name image"}).limit(10).skip(start).sort( { "create_at": -1 } )
            .then(history => {
                if(history.length === 0){
                    reject({status: 404, message: "notification Not Found !"})
                }else{
                    resolve({status: 200, messages: history});
                }

            })
            .catch(err => reject({status: 500, message: "Interval Server Error !"}));
        // detail_notify.find({"user_id" : ObjectId(user_id)})
        //
        //     .then(notifys => {
        //         if(notifys.length === 0){
        //             reject({status: 404, message: "User not found !"});
        //         }else{
        //             var arraynoti = [];
        //             var count = 0;
        //             var count2 = 0;
        //             console.log(".............size ......"+notifys.length);
        //             for(var i = 0; i <= notifys.length;i++){
        //                 if(i < (notifys.length)) {
        //                     //{"user_create": {$gt: ObjectId(user_id)},"_id": ObjectId(notifys[i].notification_id)}
        //                         //{$or: [{"_id" : ObjectId()},{"_id" : ObjectId("001001010101010101010111")},{"user_create": ObjectId(user_id)}],"number_user": {$gt: 0 }}
        //                     console.log("..................."+notifys[i].notification_id);
        //                     tbnotification.find({$or: [{"_id" : ObjectId(notifys[i].notification_id)},{"_id" : ObjectId("001001010101010101010111")},{"user_create": ObjectId(user_id)}],"number_user": {$gt: 0 }}
        //                     ,{__v:0}, function (err, data) {
        //                         if (err) {
        //                             throw err;
        //                         } else {
        //                             if(data[0]!=null){
        //                                 arraynoti.push(data);
        //                                 console.log("..12312312312 " + arraynoti.length);
        //                             }
        //                         }
        //                         count2 = count2 + 1;
        //                         if (count2 === count) {
        //                             console.log("..222222222 " + arraynoti);
        //                             resolve({status: 200, messages: arraynoti});
        //                         }
        //                     }).populate({path: "suri_id" ,select: "status"}).populate({path: "user_create" ,select: "name"});
        //
        //                     count = count + 1
        //                 }
        //             }
        //         }
        //
        //     })
        //     .catch(err => reject({status: 500, message: "Interval Server Error !"}));

    });


exports.loginOneUser = (user_id,token,version) =>
    new Promise ((resolve, reject) => {
        tbUser.find({"_id" : ObjectId(user_id)},{token : 1,name : 1,image : 1})
            .then(user => {
                if(user[0].token != token){
                    console.log("login 1 user name : "+user[0].token)
                    let  image = user[0].image;
                    let  name = user[0].name;
                    const messaged = { type_notification:"loginoneuser"};
                    var infor = { user_id: user_id, name : name, image : image};
                    send_notification.push_messtotopic(user[0].token,messaged,infor);

                    tbUser.update({"_id": ObjectId(user_id)}, {$set: {"token": token,version : version}})
                                    .then(() => {
                                        resolve({status: 200, message: "Cập nhật thành công !"});
                                    })
                }else{
                    tbUser.update({"_id": ObjectId(user_id)}, {$set: {version : version}})
                        .then(() => {
                            resolve({status: 200, message: "Cập nhật thành công version!"});
                        })

                }
            })



            .catch(err => reject({status: 500, message: "Lỗi Server !"}));
    });



// Hiển thị sanh sách token

exports.listToken = (suri_id) =>
    new Promise ((resolve, reject) => {

        suri.find({"_id" : ObjectId(suri_id)},{"list_token" : 1, "_id" : 0})

            .then(users => {
                if(users.length === 0){
                    reject({status: 404, message: "Suri Not Found !"})
                }else{
                    return users[0];
                }
            })

            .then(user => {
                resolve(user);
            })

            .catch(err => reject({status: 500, message: "Interval Server Error !"}));

    });

// Thông báo
        exports.notifySuri = (user_id,page) =>
    new Promise ((resolve, reject) => {

        detail_notify.find({"user_id" : ObjectId(user_id)},{"notification_id" : 1, "_id" : 0}).limit(10).skip(10*page)
            .sort({"create_at" : 1})
            .populate(
                {   path: "notification_id",select : {"__v" : 0},
                    populate : {path : "suri_id", select: "id status name",
                        options: {
                            sort : {"create_at" : 1}
                        }}
                }
            )

            .then(datas => {

                console.log("aaaaaaaaaa ")

                if(datas.length === 0){
                    reject({status: 404, response: 1, message : "user not found !"});
                }else{

                    // Đếm số
                    var count = datas.length;
                    // tạo mảng thông báo
                    var notiArray = [];

                    // Lặp thông báo để đẩy vào mảng notiArray
                    for(var i = 0; i < count; i++){


                        if(datas[i].notification_id.status_notify === "1"){
                            notiArray.push(datas[i].notification_id);
                        }
                    }


                    // Sắp xếp thông báo theo thời gian cập nhật
                    notiArray.sort(function(a,b){return a["create_at"] - b["create_at"]});
                    notiArray.reverse();

                    // Trả về mảng thông báo
                    return notiArray;

                }

            })

            .then(data => resolve(data))

            .catch(err => reject({status: 500, response: 2, message: "Interval Server Error !"}));

    });

        // thông báo khi đã nhập mã khuyên mãi
            // iduser: usser duoc nhap ma
            // iduser_put: user nhap ma
exports.notification_send_introduce = (iduser, iduser_put) =>
    new Promise((resolve, reject) => {
        // console.log( + " id_customer " + id_customer + " id_driver " +id_driver)
        let ObjectId = require("mongodb").ObjectId;
        tbUser.find({"_id" : ObjectId(iduser)})
            .then(id_user => {
                if(id_user.length === 0){
                    reject({status: 404, message: "không tồn tại !"});
                }else{
                    return id_user[0];
                }
            })
            .then(id_user => {



                tbUser.find({"_id": ObjectId(iduser_put)})
                    .then(iduser_put => {
                        if (iduser_put.length === 0) {
                            reject({status: 404, message: "không tồn tại !"});
                        } else {
                            return iduser_put[0];
                        }
                    }).then(iduser_put => {
                    // user được nhập mã
                    const name = id_user.name;
                    const phone_number = id_user.phone_number;
                    const token = id_user.token;
                    var message_send =  { type_notification: "introduce_send" };
                    var infor_send = {name : name, phone_number : phone_number, id_user : iduser};
                    send_notification.push_messtotopic(token,message_send,infor_send);

                    console.log("token2 nhap ma "+token +" message: "+message_send);

                    // user nhập mã
                   // const name_input = iduser_put.name;
                    const phone_input = iduser_put.phone_number;
                    const token_put = iduser_put.token;
                    var message_input = { type_notification: "introduce_input" };
                    var infor_put = {name : name, phone_number : phone_input, id_user : iduser_put};
                    send_notification.push_messtotopic(token_put,message_input,infor_put);

                    console.log("token2 nhap ma "+token_put +" message: "+message_send);

                    resolve({status: 200, token: token, messagetype : message_input, infor : infor_put});
                }).catch(err => reject({
                    status: 500, message: "Lỗi Server"}))
            })
            .catch(err => reject({
                status: 500, message: "Lỗi Server"}));
    });

// function disconnect_user(id_user) {
//
//     const d = new Date();
//     const timeStamp = d.getTime();
// // kiem tra user da ton tai trong bang chi tiet chua?
//     suri.findOne({_id: ObjectId(id_user), function (err, result) {
//             if (err) {
//                 throw err;
//             } else {
//                 console.log("...... "+ result[0].token());
//                 var message_input = { type_notification: "disconnect_user" };
//                 send_notification.push_messtotopic(token_put,message_input,infor_put);
//
//             }
//         }
//     });
//
// }

exports.disconnect_user = (phone_number,number_spam,block_user) =>
    new Promise((resolve, reject) => {
        console.log("...... "+ phone_number);
        tbUser.find({phone_number: phone_number})
            .then(result_notification => {


                console.log("...... "+ result_notification[0].token);
                const token = result_notification[0].token;
                var message_input = { type_notification: "disconnect_user" };
                var infor_put = {name : result_notification[0].name , txt_Message : "disconnect user",number_spam : number_spam, block_user:block_user};
                send_notification.push_messtotopic(token,message_input,infor_put);

                resolve({status: 200,  message: 'đã gởi thông báo thành công!'})

            }).catch(err => {

            reject({status: 500, message: 'Internal Server Error !'});
        });
    });




// count notification
exports.countNotification =(userId, count) =>
    new Promise((resolve, reject) => {
        console.log(" verrificode: usser: "+userId+" code: "+count)

        // tim kiem trong table_count_notification có trường nào trungf user_id sau đó so sánh "count"
        countNofication.find({user_id: userId})
            .then(countNofi => {

                if(countNofi.length !=0){
                    let countNoti = countNofi[0];
                    countNoti.count = count;
                    countNoti.save()
                        .then(() =>{
                            resolve({status: 201,  request_check: 0,  message: 'bạn đã update thành công!'})
                        })
                        .catch(err =>{

                            reject({status: 500, request_check: 2, message: 'Internal Server Error !'});

                        });

                }else{

                    const newCount= new countNofication({

                       user_id : userId,
                        count: count


                    });
                    newCount.save()

                        .then(() => {

                            resolve({status: 201, request_check: 0,  userid: userId, message: 'user Registered Successfully'})
                        })

                        .catch(err =>{
                            if(err.code == 11000){
                                reject({status: 409, request_check: 1, message: 'User Already Registered !'});

                            }else{
                                reject({status: 500, request_check: 2, message: 'Internal Server Error !'});
                            }
                        });

                }


            }).catch(err => {

            reject({status: 500, message: 'Internal Server Error !'});
        });
    });

exports.countChatNotification =(userId, count) =>
    new Promise((resolve, reject) => {
        console.log(" verrificode: usser: "+userId+" code: "+count)

        // tim kiem trong table_count_notification có trường nào trungf user_id sau đó so sánh "count"
        countNofication.find({user_id: userId})
            .then(countNofi => {

                if(countNofi.length !=0){
                    let countNoti = countNofi[0];
                    countNoti.count_chat = count;
                    countNoti.save()
                        .then(() =>{
                            resolve({status: 201,  request_check: 0,  message: 'bạn đã update thành công!'})
                        })
                        .catch(err =>{

                            reject({status: 500, request_check: 2, message: 'Internal Server Error !'});

                        });

                }else{

                    const newCount= new countNofication({

                        user_id : userId,
                        count_chat: count


                    });
                    newCount.save()

                        .then(() => {

                            resolve({status: 201, request_check: 0,  userid: userId, message: 'user Registered Successfully'})
                        })

                        .catch(err =>{
                            if(err.code == 11000){
                                reject({status: 409, request_check: 1, message: 'User Already Registered !'});

                            }else{
                                reject({status: 500, request_check: 2, message: 'Internal Server Error !'});
                            }
                        });

                }


            }).catch(err => {

            reject({status: 500, message: 'Internal Server Error !'});
        });
    });