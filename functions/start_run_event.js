const tbStartEvent = require('../models/table_start_event');

exports.startSuri =(check) =>
    new Promise((resolve, reject) => {

        tbStartEvent.find({})
            .then(start => {
                var d = new Date();
                var timeStamp = d.getTime();

                if(start.length == 0){
                    // userSuri: name connect with table_user
                    const newStartEvent = new tbStartEvent({

                        check_event: check,
                        created_at: timeStamp

                    });
                    newStartEvent.save()

                        .then(() => {
                            resolve({status: 200,   message: 'start Event Successfully'})
                        })

                        .catch(err =>{
                            if(err.code == 11000){
                                reject({status: 409, request_check: 1, message: 'User Already Registered !'});

                            }else{
                                reject({status: 500, request_check: 2, message: 'Internal Server Error !'});
                            }
                        });

                }else{

                    let tbEvent = start[0];
                    tbEvent.check_event = check;
                    tbEvent.save()
                        .then(()=>{
                            resolve({status: 200, check_event: check, message: 'start Event Successfully'})

                        });

                }
            })
            .catch(err =>{
                reject({status: 500, request_check: 2, message: 'Internal Server Error !'});
            });

    });

exports.responseCheckEvent =() =>
    new Promise((resolve, reject) => {

        tbStartEvent.find({})
            .then(start => {

                var booCheck = start[0].check_event;

               resolve({status: 200,check_event: booCheck, message: 'start Event Successfully'})

            })
            .catch(err =>{
                reject({status: 500, request_check: 2, message: 'Internal Server Error !'});
            });

    });