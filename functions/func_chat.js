'use strict';

const chattb = require('../models/table_chat');
const user = require('../models/table_user');
const notification = require('../functions/notification_send');
let ObjectId = require("mongodb").ObjectId;




exports.getboxMessage = (user_id,page) =>
    new Promise((resolve, reject) => {
        let limit = 10;
        if (page.is)
        if (page < 1) page = 1;
        let start = (limit * page);

        chattb.find({$or: [{idsender: ObjectId(user_id)}, {idreceiver: ObjectId(user_id)}]},
            {messages: {$slice: [-1, 1]}}).limit(10).skip(start).sort({"created_at" : -1})
            .populate({path: "idsender idreceiver", select: "_id name image"})
            .then(room => {
                if (room.length > 0) {
                    resolve({listinbox: room});
                } else {
                    resolve({status: 201, listinbox: room});
                }
            })
    });

    exports.getDataChat = (nameroom,page,idreceiver,socket) =>
        new Promise((resolve, reject) => {
        let limit = 40;
        if (page.is)
            if (page < 1) page = 1;
        let start = -(limit * page);

        chattb.find({room: nameroom}, {_id: 0,__v: 0,idreceiver: 0,idsender: 0,room: 0,messages: {$slice: [start, 40]}})
            .exec(function (err, result) {
                    if (err) {
                        throw err;
                        socket.emit("getDataMessage", [], []);

                    } else {
                            user.find({_id: ObjectId(idreceiver )},{image : 1,name : 1}, function (err, UserResult) {
                                if (err) {
                                    throw err;
                                } else {
                                    if (UserResult) {
                                        socket.emit("getDataMessage",result,UserResult);
                                        console.log( " history chat  : "+ result);
                                        console.log( " User Chat     : "+ UserResult);
                                    }
                                }
                            });
                    }
                });
        });


function insertDataMongod(nameroom, idsender, idreceiver, messageimage, messagetext, timeStamp) {
    var messages = {};
    if (messageimage != null) {
        console.log("goi tin nhan hinh anh")
        messages = {
            messageimage: messageimage,
            id_user_send: idsender,
            messagetext: null,
            created_at: timeStamp
        }
    } else if(messagetext != null){
        console.log("goi tin nhan text")
        messages = {
            messageimage: null,
            id_user_send: idsender,
            messagetext: messagetext,
            created_at: timeStamp
        }
    }else{
        console.log("goi vi tri")
        messages = {
            messageimage: null,
            id_user_send: idsender,
            messagetext: null,
            created_at: timeStamp
        }
    }

    chattb.findOne({room: nameroom}, function (err, result) {
        if (err) {
            throw err;
        } else {
            if (result) {
                // if (result.length === 0) {
                //     let chatdb = new chattb({
                //         room: nameroom,
                //         idsender: idsender,
                //         idreceiver: idreceiver,
                //         messages: messages
                //     });
                //     chatdb.save();
                //     console.log("fist create");
                // } else {
                chattb.findByIdAndUpdate(
                    result._id,
                    {$push: {"messages": messages}},

                    {safe: true, upsert: true, new: true},

                    function (err, model) {
                        console.log(err);
                    }
                );
                console.log("second create");
                // }
            } else {
                let chatdb = new chattb({
                    room: nameroom,
                    idsender: idsender,
                    idreceiver: idreceiver,
                    created_at_box: timeStamp,
                    messages: messages
                });
                chatdb.save()
                    .then(() => {
                    console.log("new create table !");
            })
            .catch(err => reject({status: 500, message: 'Error add not Sucessfully !'}))
                console.log("fist create");
            }
        }

    });
}


    exports.SendChatMessage = (nameroom, idsender, idreceiver,messageimage,messagetext,timeStamp,io) =>
    new Promise((resolve,reject) => {
        insertDataMongod(nameroom, idsender, idreceiver,messageimage,messagetext,timeStamp);

                user.find({_id : ObjectId(idsender)})
                    .then(result1 => {
                        var mResultUser = result1[0];
                        var name = mResultUser.name;
                        var image = mResultUser.image;
                        if(image != null){
                            console.log(" image != null  --- "+ image );
                        }else{
                            image = "SuriAvatar.png"
                            console.log(" image != null  --- "+ image );
                        }
                        io.to(nameroom).emit('returnmessage', { idsender : idsender,name : name, image : image,time :timeStamp ,messagetext: messagetext,messageimage : messageimage});
                    })

                    .catch(err => reject({status: 500, message: "Internal Server Error !"}));
                return true;

                })



    exports.push_notification_chat= (nameroom, idsender, idreceiver,messageimage,messagetext,timeStamp) =>
    new Promise((resolve, reject) => {
        var tokencode;
        user.find({_id: ObjectId(idsender)}, function (err, result) {
            if (err) {
                throw err;
            } else {
                var mResult = result[0];
                var namesend = mResult.name;
                var avatasend = mResult.image;
                user.find({_id: ObjectId(idreceiver)}, function (err, UserResult) {
                    if (err) {
                        throw err;
                    }else{
                        if(UserResult){
                            var mResultUser = UserResult[0];
                            tokencode = mResultUser.token;
                            if(tokencode!=null){

                            }else{

                            }

                            const message = { type_notification:"messagechat"};
                           var data ={      nameroom: nameroom,
                                            idsender: idsender,
                                            nameuser: namesend,
                                            avatar: avatasend,
                                            messageimage: messageimage,
                                            messagetext: messagetext,
                                            timeStamp: timeStamp
                                        };
                            console.log("oooooooooo  " + message.type_notification + "        " + data.messagetext)
                           notification.push_messtotopic(tokencode,message,data);
                            console.log("push mess: " + avatasend+ "  " + namesend);
                        }
                    }
                    insertDataMongod(nameroom, idsender, idreceiver,messageimage,messagetext,timeStamp);
                });

            }
        });



    });

