
const express    = require('express');
const app        = express();                
const bodyParser = require('body-parser');
const logger 	 = require('morgan');
const display_view = new require('./functions/display_view'); // update
const schedule = require("node-schedule"); // package for update view posts
const router 	 = express.Router();
const port 	     = process.env.PORT || 8070;
const server = require("http").createServer(app);
const io         = require("socket.io")(server);

// Auto update view
const j = schedule.scheduleJob("50 * * * *", function () {
    console.log("added view to posts");
    display_view.mUpView()
    // console.log("The answer to life, the universe, and everything!");
});


app.use(bodyParser.json());
app.use(logger('dev'));

require('./router')(router);
require('./socketio.js')(io);
app.use('/api/suri', router);


server.listen(port, function () {
    console.log('Server listening at port %d', port);
});